package Final_Exam.dao;

import Final_Exam.model.Computer;

import java.util.ArrayList;

public interface ComputerDAO {

    ArrayList<Computer> displayAllComputer();
    void addComputer (Computer computer);
    boolean updateComputer(int id, ArrayList<Computer> computers, Computer computer);
    boolean removeComputer(int id, ArrayList<Computer> computers);
}
