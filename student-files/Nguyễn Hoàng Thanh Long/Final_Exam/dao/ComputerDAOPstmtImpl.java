package Final_Exam.dao;

import Final_Exam.model.Computer;
import Final_Exam.model.Laptop;
import Final_Exam.model.PC;
import Final_Exam.utils.SqlHelper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ComputerDAOPstmtImpl implements ComputerDAO {

    @Override
    public ArrayList<Computer> displayAllComputer() {
        // TODO: tạo quá nhiều bảng, em chỉ đơn giản tạo 1 bảng là được
        ArrayList<Computer> computers = new ArrayList<>();
        String sql = "select * from computer";
        String sql1 = "select * from laptop";
        String sql2 = "select * from pc";
        try (Connection conn1 = SqlHelper.getConnection();
             PreparedStatement statement1 = conn1.prepareStatement(sql1)) {
            ResultSet resultSetCP = statement1.executeQuery();
            while (resultSetCP.next()) {
                int id = resultSetCP.getInt("id");
                String name = resultSetCP.getString("name");
                byte ram = resultSetCP.getByte("ram");
                float cpu = resultSetCP.getFloat("cpu");
                short hdd = resultSetCP.getShort("hdd");

                Computer computer = new Computer(id, name, ram, cpu, hdd);
                computers.add(computer);
            }
        }
        catch (SQLException e){
            e.printStackTrace();
        }
        try (Connection conn2 = SqlHelper.getConnection();
             PreparedStatement statement2 = conn2.prepareStatement(sql1)) {
            ResultSet resultSetLaptop = statement2.executeQuery();
            while (resultSetLaptop.next()) {
                int id = resultSetLaptop.getInt("id");
                String name = resultSetLaptop.getString("name");
                byte ram = resultSetLaptop.getByte("ram");
                float cpu = resultSetLaptop.getFloat("cpu");
                short hdd = resultSetLaptop.getShort("hdd");
                float weight = resultSetLaptop.getFloat("weight");
                String color = resultSetLaptop.getString("color");
                Laptop laptop = new Laptop(id, name, ram, cpu, hdd, weight, color);
                computers.add(laptop);
            }
        }
        catch (SQLException e){
            e.printStackTrace();
        }
        try (Connection conn2 = SqlHelper.getConnection();
             PreparedStatement statement2 = conn2.prepareStatement(sql2)) {
            ResultSet resultSetPC = statement2.executeQuery();
            while (resultSetPC.next()) {
                int id = resultSetPC.getInt("id");
                String name = resultSetPC.getString("name");
                byte ram = resultSetPC.getByte("ram");
                float cpu = resultSetPC.getFloat("cpu");
                short hdd = resultSetPC.getShort("hdd");
                String graphicsCardName = resultSetPC.getString("graphics_card_name");
                int vramCapacity = resultSetPC.getInt("vram_capacity");
                PC pc = new PC(id, name, ram, cpu, hdd, graphicsCardName, vramCapacity);
                computers.add(pc);
            }
        }
        catch (SQLException e){
            e.printStackTrace();
        }
        return computers;
    }

    @Override
    public void addComputer(Computer computer) {
        String sql = "insert into computer(id, name, ram, cpu, hdd) values(?,?,?,?,?)";
        String sql1 = "insert into pc(name, ram, cpu, hdd, graphics_card_name,vram_capacity) values(?,?,?,?,?,?)";
        String sql2 = "insert into laptop(name, ram, cpu, hdd, weight, color) values(?,?,?,?,?,?)";

        // TODO: check cái gì đây em
        if (computer == computer) {
            try (Connection conn = SqlHelper.getConnection();
                 PreparedStatement statement = conn.prepareStatement(sql)) {
                statement.setInt(1, computer.getId());

                statement.setString(2, computer.getName());

                statement.setByte(3, computer.getRam());

                statement.setFloat(4, computer.getCpu());

                statement.setShort(5, computer.getHdd());
                int rowAffected = statement.executeUpdate();
                System.out.println("Add Computer Successfully!");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (computer instanceof Laptop laptop){
            try (Connection conn = SqlHelper.getConnection();
                 PreparedStatement statement = conn.prepareStatement(sql1)) {
                statement.setInt(1, laptop.getId());

                statement.setString(2, laptop.getName());

                statement.setByte(3, laptop.getRam());

                statement.setFloat(4, laptop.getCpu());

                statement.setShort(5, laptop.getHdd());

                statement.setFloat(6, laptop.getWeight());

                statement.setString(7, laptop.getColor());
                int rowAffected = statement.executeUpdate();
                System.out.println("Add Laptop Successfully!");

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (computer instanceof PC pc){
            try (Connection conn = SqlHelper.getConnection();
                 PreparedStatement statement = conn.prepareStatement(sql2)) {
                statement.setInt(1, pc.getId());

                statement.setString(2, pc.getName());

                statement.setByte(3, pc.getRam());

                statement.setFloat(4, pc.getCpu());

                statement.setShort(5, pc.getHdd());

                statement.setString(6, pc.getGraphicsCardName());

                statement.setInt(7, pc.getVramCapacity());
                int rowAffected = statement.executeUpdate();
                System.out.println("Add PC Successfully!");

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean updateComputer(int id, ArrayList<Computer> computers, Computer computer) {
        return false;
    }

    @Override
    public boolean removeComputer(int id, ArrayList<Computer> computers) {
        return false;
    }
}
