package Final_Exam.model;

public class PC extends Computer{
    private String graphicsCardName;
    private int vramCapacity;

    public PC(int id, String name, byte ram, float cpu, short hdd, String graphicsCardName, int vramCapacity) {
        super(id, name, ram, cpu, hdd);
        this.graphicsCardName = graphicsCardName;
        this.vramCapacity = vramCapacity;
    }

    public PC(String name, byte ram, float cpu, short hdd, String graphicsCardName, int vramCapacity) {
        super(name, ram, cpu, hdd);
        this.graphicsCardName = graphicsCardName;
        this.vramCapacity = vramCapacity;
    }

    public String getGraphicsCardName() {
        return graphicsCardName;
    }

    public void setGraphicsCardName(String graphicsCardName) {
        this.graphicsCardName = graphicsCardName;
    }

    public int getVramCapacity() {
        return vramCapacity;
    }

    public void setVramCapacity(int vramCapacity) {
        this.vramCapacity = vramCapacity;
    }

    @Override
    public String toString() {
        return "PC{" + super.toString() +
                "graphicsCardName='" + graphicsCardName + '\'' +
                ", vramCapacity=" + vramCapacity +
                "} " ;
    }
}
