package Final_Exam.app;

import Final_Exam.model.Computer;
import Final_Exam.model.Laptop;
import Final_Exam.model.PC;
import Final_Exam.utils.Validator;

import java.util.*;

public class DeviceList {
    private ArrayList<Computer> devices;

    public DeviceList(ArrayList<Computer> devices) {
        this.devices = devices;
    }

    public static void main(String[] args) {
        // TODO: em nhờ chat gpt code à
        ArrayList<Computer> devices = new ArrayList<>();
        DeviceList deviceList = new DeviceList(devices);
        Scanner scanner = new Scanner(System.in);
        int choice;
        do {
            System.out.println("====== Device Management ======");
            System.out.println("1. Create");
            System.out.println("2. Display all");
            System.out.println("3. Update");
            System.out.println("4. Delete");
            System.out.println("5. Search by name");
            System.out.println("6. Quit");
            System.out.print("Please choose function you'd like to do: ");
            choice = scanner.nextInt();
            scanner.nextLine(); // Consume newline
            switch (choice) {
                case 1:
                    deviceList.add(scanner);
                    break;
                case 2:
                    deviceList.display();
                    break;
                case 3:
                    deviceList.update(scanner);
                    break;
                case 4:
                    deviceList.remove(scanner);
                    break;
                case 5:
                    deviceList.find(scanner);
                    break;
                case 6:
                    System.out.println("Exiting...");
                    break;
                default:
                    System.out.println("Invalid choice.");
            }
        } while (choice != 6);
        scanner.close();
    }

    public void add(Scanner scanner) {
        System.out.println("Adding a new device:");
        System.out.println("Choose device type:");
        System.out.println("1. PC");
        System.out.println("2. Laptop");
        int choice = scanner.nextInt();
        scanner.nextLine(); // Consume newline
        System.out.print("Enter name: ");
        String name = scanner.nextLine();
        if (!Validator.isValidName(name)) {
            return; // Stop adding if name is not valid
        }
        System.out.print("Enter RAM: ");
        byte ram = scanner.nextByte();
        if (!Validator.isValidRam(ram)) {
            return; // Stop adding if RAM is not valid
        }
        System.out.print("Enter CPU: ");
        float cpu = scanner.nextFloat();
        if (!Validator.isValidCpu(cpu)) {
            return; // Stop adding if CPU is not valid
        }
        System.out.print("Enter HDD: ");
        short hdd = scanner.nextShort();
        if (!Validator.isValidHdd(hdd)) {
            return; // Stop adding if HDD is not valid
        }
        scanner.nextLine(); // Consume newline
        if (choice == 1) {
            System.out.print("Enter graphics card name: ");
            String graphicsCardName = scanner.nextLine();
            System.out.print("Enter VRAM capacity: ");
            int vramCapacity = scanner.nextInt();
            scanner.nextLine(); // Consume newline
            devices.add(new PC(name, ram, cpu, hdd, graphicsCardName, vramCapacity));
            System.out.println("PC added successfully.");
        } else if (choice == 2) {
            System.out.print("Enter weight: ");
            float weight = scanner.nextFloat();
            if (!Validator.isValidWeight(weight)) {
                return; // Stop adding if weight is not valid
            }
            scanner.nextLine(); // Consume newline
            System.out.print("Enter color: ");
            String color = scanner.nextLine();
            if (!Validator.isValidColor(color)) {
                return; // Stop adding if color is not valid
            }
            devices.add(new Laptop(name, ram, cpu, hdd, weight, color));
            System.out.println("Laptop added successfully.");
        } else {
            System.out.println("Invalid choice.");
        }
    }

    public void sortByName() {
        // Tạo một bộ so sánh để sắp xếp thiết bị theo tên trong thứ tự tăng dần
        Comparator<Computer> nameComparator = Comparator.comparing(Computer::getName);

        // Tạo một bộ so sánh để sắp xếp thiết bị theo ID trong thứ tự tăng dần
        Comparator<Computer> idComparator = Comparator.comparing(Computer::getId);

        // Sắp xếp danh sách thiết bị trước theo tên trong thứ tự tăng dần và sau đó theo ID trong thứ tự tăng dần
        // Nếu có hai thiết bị có tên trùng nhau, thì sẽ sắp xếp tiếp theo ID
        devices.sort(nameComparator.thenComparing(idComparator));
    }

    public void display() {
        sortByName();
        System.out.println("List of devices:");
        for (Computer device : devices) {
            System.out.println(device);
        }
    }

    public void update(Scanner scanner) {
        System.out.print("Enter the id of the device to update: ");
        int id = scanner.nextInt();
        scanner.nextLine(); // Consume newline
        boolean found = false;
        for (int i = 0; i < devices.size(); i++) {
            Computer device = devices.get(i);
            if (device.getId() == id) {
                if (device instanceof PC) {
                    PC pc = (PC) device;
                    System.out.print("Enter new graphics card name: ");
                    String graphicsCardName = scanner.nextLine();
                    if (!Validator.isValidName(graphicsCardName)) {
                        return; // Stop updating if graphics card name is not valid
                    }
                    System.out.print("Enter new VRAM capacity: ");
                    int vramCapacity = scanner.nextInt();
                    if (!Validator.isValidRam((byte) vramCapacity)) {
                        return; // Stop updating if VRAM capacity is not valid
                    }
                    scanner.nextLine(); // Consume newline
                    pc.setGraphicsCardName(graphicsCardName);
                    pc.setVramCapacity(vramCapacity);
                } else if (device instanceof Laptop) {
                    Laptop laptop = (Laptop) device;
                    System.out.print("Enter new weight: ");
                    float weight = scanner.nextFloat();
                    if (!Validator.isValidWeight(weight)) {
                        return; // Stop updating if weight is not valid
                    }
                    scanner.nextLine(); // Consume newline
                    System.out.print("Enter new color: ");
                    String color = scanner.nextLine();
                    if (!Validator.isValidColor(color)) {
                        return; // Stop updating if color is not valid
                    }
                    laptop.setWeight(weight);
                    laptop.setColor(color);
                }
                System.out.println("Device updated successfully.");
                found = true;
                break;
            }
        }
        if (!found) {
            System.out.println("Device does not exist.");
        }
    }

    public void remove(Scanner scanner) {
        System.out.print("Enter the id of the device to remove: ");
        int id = scanner.nextInt();
        scanner.nextLine(); // Consume newline
        boolean removed = false;

        Computer deviceToRemove = null;
        for (Computer device : devices) {
            if (device.getId() == id) {
                deviceToRemove = device;
                break;
            }
        }

        if (deviceToRemove != null) {
            System.out.print("Are you sure you want to remove the device with ID " + id + " (Y/N)? ");
            String confirm = scanner.nextLine();

            if (confirm.equalsIgnoreCase("Y")) {
                devices.remove(deviceToRemove);
                System.out.println("Device removed successfully.");
                removed = true;
            } else {
                System.out.println("Deletion canceled.");
            }
        } else {
            System.out.println("Device does not exist.");
        }
    }

    public void find(Scanner scanner) {
        System.out.print("Enter a search string to find matching devices: ");
        String searchString = scanner.nextLine();
        List<Computer> matchingDevices = new ArrayList<>();

        for (Computer device : devices) {
            if (device.getName().toLowerCase().contains(searchString.toLowerCase())) {
                matchingDevices.add(device);
            }
        }

        if (matchingDevices.isEmpty()) {
            System.out.println("No devices found with the specified search string.");
        } else {
            // Sort devices by name before displaying
            Collections.sort(matchingDevices, (d1, d2) -> d1.getName().compareToIgnoreCase(d2.getName()));

            System.out.println("Devices found: ");
            for (Computer device : matchingDevices) {
                System.out.println(device);
            }
        }
    }
}


