package Final_Exam.utils;

import Final_Exam.exception.ValidationException;
import Final_Exam.model.Computer;
import Final_Exam.model.Laptop;

import java.util.ArrayList;
import java.util.List;

public class Validator {
    public static boolean isIdDuplicate(int id, ArrayList<Computer> devices) {
        for (Computer device : devices) {
            if (device.getId() == id) {
                System.out.println("ID already exists. Please enter a unique ID.");
                return true;
            }
        }
        return false;
    }

    public static boolean isValidName(String name) {
        if (name == null || name.isEmpty() || name.length() < 5) {
            System.out.println("Name must not be empty and must be at least five characters.");
            return false;
        }
        return true;
    }

    public static boolean isValidRam(byte ram) {
        if (ram < 2 || ram > 128) {
            System.out.println("RAM must be an integer number between 2 and 128.");
            return false;
        }
        return true;
    }

    public static boolean isValidCpu(float cpu) {
        if (cpu < 1 || cpu > 10) {
            System.out.println("CPU must be a number between 1 and 10.");
            return false;
        }
        return true;
    }

    public static boolean isValidHdd(short hdd) {
        if (hdd < 128) {
            System.out.println("HDD must be an integer number at least 128.");
            return false;
        }
        return true;
    }

    public static boolean isValidWeight(float weight) {
        if (weight < 0.5 || weight > 8) {
            System.out.println("Weight must be a number between 0.5 and 8.");
            return false;
        }
        return true;
    }

    public static boolean isValidColor(String color) {
        if (color != null && !color.isEmpty()) {
            String[] validColors = {"red", "white", "blue", "gray"};
            for (String validColor : validColors) {
                if (color.equalsIgnoreCase(validColor)) {
                    return true; // Màu hợp lệ
                }
            }
            // Nếu không khớp với bất kỳ màu nào trong danh sách
            System.out.println("Color must be red, white, blue, or gray.");
        } else {
            // Nếu màu rỗng
            System.out.println("Color must not be empty.");
        }
        return false; // Màu không hợp lệ
    }
}
