import dao.ComputerDao;
import dao.ComputerDaoImpl;
import model.Computer;
import service.ComputerService;
import service.ComputerServiceImpl;
import utils.InputData;

import java.util.ArrayList;
import java.util.Scanner;

public class DeviceList {
    static ArrayList<Computer> devices = new ArrayList<>();
    private ComputerService computerService;
    private ComputerDao computerDao;
    Scanner sc = new Scanner(System.in);
    public DeviceList(ArrayList<Computer> devices) {
        DeviceList.devices = devices;
        this.computerDao = new ComputerDaoImpl();
        this.computerService = new ComputerServiceImpl(computerDao);
    }
    public Computer createComputer(){
        Computer computer = InputData.inputData(sc);
        devices.add(computer);
        System.out.println("Create successfully");
        return computer;
    }
    public void addComputer(){
        System.out.print("Enter id to add computer: ");
        int id = sc.nextInt();
        for (Computer device : devices){
            if (device.getId() == id){
                computerService.addComputer(device);
            }
        }
    }

    public void displayAllComputer(){
        ArrayList<Computer> computers = computerService.displayAllComputer();
        if (computers.isEmpty()){
            System.out.println("No computers found");
            return;
        }
        computerService.sortByName(computers);
        for (Computer computer : computers) {
            System.out.println(computer.toString());
        }
    }

    public void removeComputer(){
        System.out.print("Enter id to remove computer: ");
        int id = sc.nextInt();
        sc.nextLine();
        if (computerService.checkIDExisted(id)){
            if(computerService.removeComputer(id)){
                System.out.println("remove successfully");
            }
            else System.out.println("remove failed");
        }
        else System.out.println("ID does not exist");
    }


    public void updateComputer(){
        System.out.print("Enter id to update computer: ");
        int id = sc.nextInt();
        sc.nextLine();
        if (computerService.checkIDExisted(id)){
            if(computerService.updateComputer(id, InputData.inputDataToUpdate(sc,id))){
                System.out.println("Update successfully");
            }
            else System.out.println("Update failed");
        }
        else System.out.println("ID does not exist");
    }
    public void findComputer(){
        System.out.print("Enter name to find computer: ");
        String name = sc.nextLine();
        computerService.findComputer(name);
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        DeviceList deviceList = new DeviceList(devices);
        boolean running = true;
        while (running){
            displayMenu();
            String userInput = scanner.nextLine();
            try {
                int choice = Integer.parseInt(userInput);
                switch (choice){
                    case 1:
                        deviceList.createComputer();
                        break;
                    case 2:
                        deviceList.displayAllComputer();
                        break;
                    case 3:
                        deviceList.updateComputer();
                        break;
                    case 4:
                        deviceList.removeComputer();
                        break;
                    case 5:
                        deviceList.findComputer();
                        break;
                    case 6:
                        deviceList.addComputer();
                        break;
                    case 7:
                        running = false;
                        System.out.println("Exit.");
                        break;
                    default:
                        System.out.println("Invalid choice. Please try again.");
                        break;
                }
            } catch (NumberFormatException e){
                e.printStackTrace();
            }

        }
        scanner.close();

    }

    private static void displayMenu() {
        System.out.println("\n Menu");
        System.out.println("======Device Management======");
        System.out.println("1. Create");
        System.out.println("2. Display all");
        System.out.println("3. Update");
        System.out.println("4. Delete");
        System.out.println("5. Search by name");
        System.out.println("6. Save to database");
        System.out.println("7. Exit");
        System.out.print("Please choose function you'd like to do: ");
    }

}
