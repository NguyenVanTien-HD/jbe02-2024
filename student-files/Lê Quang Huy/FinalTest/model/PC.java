package model;

public class PC extends Computer{
    public PC(int id, String name, byte ram, float cpu, short hdd) {
        super(id, name, ram, cpu, hdd);
    }
    @Override
    public String toString() {
        return "PC{" +
                "id=" + super.getId() +
                ", name='" + super.getName() + '\'' +
                ", ram=" + super.getRam() +
                ", cpu=" + super.getCpu() +
                ", hdd=" + super.getHdd() +
                '}';
    }
}
