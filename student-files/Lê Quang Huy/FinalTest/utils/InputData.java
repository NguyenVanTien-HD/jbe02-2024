package utils;

import dao.ComputerDao;
import dao.ComputerDaoImpl;
import exception.ValidateException;
import model.Computer;
import model.Laptop;
import model.PC;
import validate.ValidateData;

import java.util.InputMismatchException;
import java.util.Scanner;

public class InputData {
    static private ComputerDao computerDao = new ComputerDaoImpl();
    public static Computer inputData(Scanner scanner) {
        // Todo: nen clean code đoạn này nhé. split cac block này thành method nha
        // Todo: em ném ra lỗi rồi bắt ngay ở dưới. việc thow ra gần như không có ý nghĩa nhé (các phần ở dưới)
        int id = 0;
        while (true) {
            try {
                System.out.print("Enter computer's id: ");
                id = scanner.nextInt();
                if (!ValidateData.validateId(id, computerDao.displayAllComputer())){
                    throw new ValidateException("Data is duplicated");
                }
                scanner.nextLine();
                break;
            } catch (ValidateException e) {
                System.out.println(e.getMessage());
                scanner.nextLine();
            }
            catch (InputMismatchException e) {
                System.out.println("Invalid input. Please enter an integer.");
                scanner.nextLine();
            }
        }

        String name = "";
        while (true) {
            try {
                System.out.print("Enter computer's name: ");
                name = scanner.nextLine();
                if (!ValidateData.validateName(name)) {
                    throw new ValidateException("Name must be at least five characters");
                }
                break;
            } catch (ValidateException e) {
                System.out.println(e.getMessage());
            }
        }

        byte ram = 0;
        while (true) {
            try {
                System.out.print("Enter computer's ram: ");
                ram = scanner.nextByte();
                if (!ValidateData.validateRam(ram)) {
                    throw new ValidateException("Ram from 2 to 128");
                }
                break;
            } catch (ValidateException e) {
                System.out.println(e.getMessage());
                scanner.nextLine();
            }
            catch (InputMismatchException e) {
                System.out.println("Invalid input. Ram from 2 to 128.");
                scanner.nextLine();
            }
        }

        float cpu = 0;
        while (true) {
            try {
                System.out.print("Enter computer's cpu: ");
                cpu = scanner.nextFloat();
                if (!ValidateData.validateCpu(cpu)) {
                    throw new ValidateException("CPU from 1 to 10");
                }
                break;
            } catch (ValidateException e) {
                System.out.println(e.getMessage());
                scanner.nextLine();
            }
            catch (InputMismatchException e) {
                System.out.println("Invalid input. CPU from 1 to 10.");
                scanner.nextLine();
            }
        }

        short hdd = 0;
        while (true) {
            try {
                System.out.print("Enter computer's hdd: ");
                hdd = scanner.nextShort();
                if (!ValidateData.validateHdd(hdd)) {
                    throw new ValidateException("HDD from 128");
                }
                break;
            } catch (ValidateException e) {
                System.out.println(e.getMessage());
                scanner.nextLine();
            }
            catch (InputMismatchException e) {
                System.out.println("Invalid input. HDD from 128.");
                scanner.nextLine();
            }
        }

        int choose = 0;
        while (true) {
            try {
                System.out.print("Choose 1 to create Laptop ------ Choose 2 to create PC: ");
                choose = scanner.nextInt();
                if (choose != 1 && choose != 2) {
                    throw new ValidateException("Invalid choice, please enter 1 or 2");
                }
                scanner.nextLine();
                break;
            } catch (ValidateException e) {
                System.out.println(e.getMessage());
                scanner.nextLine();
            }
        }

        if (choose == 2) {
            return new PC(id, name, ram, cpu, hdd);
        }

        float weight = 0;
        while (true) {
            try {
                System.out.print("Enter computer's weight: ");
                weight = scanner.nextFloat();
                if (!ValidateData.validateWeight(weight)) {
                    throw new ValidateException("Weight is number and ranges from 0.5 to 8");
                }
                scanner.nextLine();
                break;
            } catch (ValidateException e) {
                System.out.println(e.getMessage());
                scanner.nextLine();
            }
            catch (InputMismatchException e) {
                System.out.println("Invalid input. Weight from 0.5 to 8.");
                weight = scanner.nextFloat();
            }
        }

        String color = "";
        while (true) {
            try {
                System.out.print("Enter computer's color: ");
                color = scanner.nextLine();
                if (!ValidateData.validateColor(color)) {
                    throw new ValidateException("Color is not valid");
                }
                break;
            } catch (ValidateException e) {
                System.out.println(e.getMessage());
            }
        }

        return new Laptop(id, name, ram, cpu, hdd, weight, color);
    }

    public static Computer inputDataToUpdate(Scanner scanner, int id) {
        // TODO: comment như method bên trên. duplicate code luôn nè
        String name = "";
        while (true) {
            try {
                System.out.print("Enter computer's name: ");
                name = scanner.nextLine();
                if (!ValidateData.validateName(name)) {
                    throw new ValidateException("Name must be at least five characters");
                }
                break;
            } catch (ValidateException e) {
                System.out.println(e.getMessage());
            }
        }

        byte ram = 0;
        while (true) {
            try {
                System.out.print("Enter computer's ram: ");
                ram = scanner.nextByte();
                if (!ValidateData.validateRam(ram)) {
                    throw new ValidateException("Ram from 2 to 128");
                }
                break;
            } catch (ValidateException e) {
                System.out.println(e.getMessage());
                scanner.nextLine();
            } catch (InputMismatchException e) {
                System.out.println("Invalid input. Ram from 2 to 128.");
                scanner.nextLine();
            }
        }

        float cpu = 0;
        while (true) {
            try {
                System.out.print("Enter computer's cpu: ");
                cpu = scanner.nextFloat();
                if (!ValidateData.validateCpu(cpu)) {
                    throw new ValidateException("CPU from 1 to 10");
                }
                break;
            } catch (ValidateException e) {
                System.out.println(e.getMessage());
                scanner.nextLine();
            } catch (InputMismatchException e) {
                System.out.println("Invalid input. CPU from 1 to 10.");
                scanner.nextLine();
            }
        }

        short hdd = 0;
        while (true) {
            try {
                System.out.print("Enter computer's hdd: ");
                hdd = scanner.nextShort();
                if (!ValidateData.validateHdd(hdd)) {
                    throw new ValidateException("HDD from 128");
                }
                break;
            } catch (ValidateException e) {
                System.out.println(e.getMessage());
                scanner.nextLine();
            } catch (InputMismatchException e) {
                System.out.println("Invalid input. HDD from 128.");
                scanner.nextLine();
            }
        }

        System.out.print("Choose 1 to create Laptop ------ Choose 2 to create PC: ");
        int choose = scanner.nextInt();
        scanner.nextLine();
        if (choose == 2) {
            return new PC(id, name, ram, cpu, hdd);
        }

        float weight = 0;
        while (true) {
            try {
                System.out.print("Enter computer's weight: ");
                weight = scanner.nextFloat();
                if (!ValidateData.validateWeight(weight)) {
                    throw new ValidateException("Weight is number and ranges from 0.5 to 8");
                }
                scanner.nextLine();
                break;
            } catch (ValidateException e) {
                System.out.println(e.getMessage());
                scanner.nextLine();
            }
        }

        String color = "";
        while (true) {
            try {
                System.out.print("Enter computer's color: ");
                color = scanner.nextLine();
                if (!ValidateData.validateColor(color)) {
                    throw new ValidateException("Color is not valid");
                }
                break;
            } catch (ValidateException e) {
                System.out.println(e.getMessage());
            }
        }

        return new Laptop(id, name, ram, cpu, hdd, weight, color);
    }
}
