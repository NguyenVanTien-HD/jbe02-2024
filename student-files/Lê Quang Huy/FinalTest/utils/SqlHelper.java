package utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class SqlHelper {
    private SqlHelper() {
    }

    public static Connection getConnection() throws SQLException {
        String url = "jdbc:mysql://localhost:3306/finaltest";
        String username = "root";
        String pass = "huy112005";

        return DriverManager.getConnection(url, username, pass);
    }
}
