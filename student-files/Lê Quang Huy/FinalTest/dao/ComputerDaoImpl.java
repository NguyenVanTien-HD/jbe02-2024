package dao;
import model.*;
import utils.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ComputerDaoImpl implements ComputerDao {
    @Override
    public ArrayList<Computer> displayAllComputer() {
        ArrayList<Computer> computers = new ArrayList<>();
        String sql1 = "select * from pc";
        String sql2 = "select * from laptop";
        try (Connection conn1 = SqlHelper.getConnection();
             PreparedStatement statement1 = conn1.prepareStatement(sql1)) {
            ResultSet resultSetPC = statement1.executeQuery();
            while (resultSetPC.next()) {
                int id = resultSetPC.getInt("id");
                String name = resultSetPC.getString("name");
                byte ram = resultSetPC.getByte("ram");
                float cpu = resultSetPC.getFloat("cpu");
                short hdd = resultSetPC.getShort("hdd");

                PC customer = new PC(id, name, ram, cpu, hdd);
                computers.add(customer);
            }
        }
            catch (SQLException e){
            e.printStackTrace();
            }
        try (Connection conn2 = SqlHelper.getConnection();
             PreparedStatement statement2 = conn2.prepareStatement(sql2)) {
            ResultSet resultSetLaptop = statement2.executeQuery();
            while (resultSetLaptop.next()) {
                int id = resultSetLaptop.getInt("id");
                String name = resultSetLaptop.getString("name");
                byte ram = resultSetLaptop.getByte("ram");
                float cpu = resultSetLaptop.getFloat("cpu");
                short hdd = resultSetLaptop.getShort("hdd");
                float weight = resultSetLaptop.getFloat("weight");
                String color = resultSetLaptop.getString("color");
                Laptop laptop = new Laptop(id, name, ram, cpu, hdd, weight, color);
                computers.add(laptop);
            }
        }
            catch (SQLException e){
            e.printStackTrace();
            }
        return computers;
    }

    @Override
    public void addComputer(Computer computer) {
        String sql1 = "insert into pc(id, name, ram, cpu, hdd) values(?,?,?,?,?)";
        String sql2 = "insert into laptop(id, name, ram, cpu, hdd, weight, color) values(?,?,?,?,?,?,?)";
        if (computer instanceof PC) {
            try (Connection conn = SqlHelper.getConnection();
                 PreparedStatement statement = conn.prepareStatement(sql1)) {
            statement.setInt(1, computer.getId());

            statement.setString(2, computer.getName());

            statement.setByte(3, computer.getRam());

            statement.setFloat(4, computer.getCpu());

            statement.setShort(5, computer.getHdd());
            int count = statement.executeUpdate();
            System.out.println("Add PC Successfully!");
        } catch (SQLException e) {
            e.printStackTrace();
        }}
        if (computer instanceof Laptop laptop){
            try (Connection conn = SqlHelper.getConnection();
                 PreparedStatement statement = conn.prepareStatement(sql2)) {
                statement.setInt(1, laptop.getId());

                statement.setString(2, laptop.getName());

                statement.setByte(3, laptop.getRam());

                statement.setFloat(4, laptop.getCpu());

                statement.setShort(5, laptop.getHdd());

                statement.setFloat(6, laptop.getWeight());

                statement.setString(7, laptop.getColor());
                int count = statement.executeUpdate();
                System.out.println("Add Laptop Successfully!");

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
    @Override
    public boolean updateComputer(int id,ArrayList<Computer> computers,Computer computerUpdate) {
        boolean success = false;
        String sql1 = "update pc set name = ?, ram = ?, cpu = ?, hdd = ? where id = ?";
        String sql2 = "update laptop set name = ?, ram = ?, cpu = ?, hdd = ?, weight = ?, color = ? where id = ?";

        // TODO: update theo id rồi em duyệt hết dữ liệu làm gì nhỉ?
        // TODO: nếu dữ liệu table lớn thì toi à?
        for (Computer computer : computers) {
            if (computer.getId() == id) {
                if (computerUpdate instanceof PC && computer instanceof PC){
                    try(Connection conn = SqlHelper.getConnection();
                        PreparedStatement statement = conn.prepareStatement(sql1)){
                        statement.setString(1, computerUpdate.getName());
                        statement.setByte(2, computerUpdate.getRam());
                        statement.setFloat(3, computerUpdate.getCpu());
                        statement.setShort(4, computerUpdate.getHdd());
                        statement.setInt(5, id);
                        int rowsDeleted = statement.executeUpdate();
                        success = rowsDeleted > 0;
                    } catch (SQLException e){
                        e.printStackTrace();
                    }
                }
                if (computerUpdate instanceof Laptop && computer instanceof Laptop){
                    Laptop laptop = (Laptop) computerUpdate;
                    try(Connection conn = SqlHelper.getConnection();
                        PreparedStatement statement = conn.prepareStatement(sql2)){
                            statement.setString(1, laptop.getName());
                            statement.setByte(2, laptop.getRam());
                            statement.setFloat(3, laptop.getCpu());
                            statement.setShort(4, laptop.getHdd());
                            statement.setFloat(5,laptop.getWeight());
                            statement.setString(6,laptop.getColor());
                            statement.setInt(7,id);
                            int rowsDeleted = statement.executeUpdate();
                            success = rowsDeleted > 0;
                    } catch (SQLException e){
                        e.printStackTrace();
                    }
                }
            }
        }
        return success;
    }
    @Override
    public boolean removeComputer(int id,ArrayList<Computer> computers) {
        boolean success = false;
        String sql1 = "delete from pc where id = ?";
        String sql2 = "delete from laptop where id = ?";

        // TODO: chỗ này giống phần update ấy em. không nên làm thế này tí nào
        for (Computer computer : computers) {
            if (computer.getId() == id) {
                if (computer instanceof PC){
                    try(Connection conn = SqlHelper.getConnection();
                        PreparedStatement statement = conn.prepareStatement(sql1)){
                        statement.setInt(1, id);
                        int rowsDeleted = statement.executeUpdate();
                        success = rowsDeleted > 0;
                    } catch (SQLException e){
                        e.printStackTrace();
                    }
                }
                if (computer instanceof Laptop){
                    try(Connection conn = SqlHelper.getConnection();
                        PreparedStatement statement = conn.prepareStatement(sql2)){
                        statement.setInt(1, id);
                        int rowsDeleted = statement.executeUpdate();
                        success = rowsDeleted > 0;
                    } catch (SQLException e){
                        e.printStackTrace();
                    }
                }
            }
        }
        return success;
    }
}

