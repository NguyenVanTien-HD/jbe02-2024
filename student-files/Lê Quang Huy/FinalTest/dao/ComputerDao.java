package dao;
import model.*;
import java.util.*;

public interface ComputerDao {
    ArrayList<Computer> displayAllComputer();
    void addComputer (Computer computer);
    boolean updateComputer(int id,ArrayList<Computer> computers,Computer computer);
    boolean removeComputer(int id, ArrayList<Computer> computers);
}