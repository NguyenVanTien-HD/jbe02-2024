package service;

import model.Computer;

import java.util.ArrayList;

public interface ComputerService {
    ArrayList<Computer> displayAllComputer();
    void addComputer (Computer computer);
    boolean updateComputer(int id, Computer computer);
    boolean removeComputer(int id);
    void findComputer(String name);
    void sortByName(ArrayList<Computer> computers);
    boolean checkIDExisted(int id);
}
