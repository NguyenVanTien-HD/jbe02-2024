package service;

import dao.ComputerDao;
import model.Computer;

import java.util.ArrayList;
import java.util.Collections;

public class ComputerServiceImpl implements ComputerService{

    private ComputerDao computerDao;

    public ComputerServiceImpl(ComputerDao computerDao) {
        this.computerDao = computerDao;
    }

    @Override
    public ArrayList<Computer> displayAllComputer() {
        return computerDao.displayAllComputer();
    }
    @Override
    public void addComputer (Computer computer){
        computerDao.addComputer(computer);
    }
    @Override
    public boolean updateComputer(int id, Computer computer){
        return computerDao.updateComputer(id, computerDao.displayAllComputer(),computer);
    }
    @Override
    public boolean removeComputer(int id){
        return computerDao.removeComputer(id,computerDao.displayAllComputer());
    }
    @Override
    public void findComputer(String name){
        ArrayList<Computer> computers = computerDao.displayAllComputer();
        // TODO: Thiếu sắp xếp theo tên và tìm kiếm đang tìm tuyệt đối rồi, chứ không phải tìm tương đối
        for(Computer computer : computers){
            if(computer.getName().equals(name)){
                System.out.println(computer);
            }
        }
    }
    @Override
    public void sortByName(ArrayList<Computer> computers) {
       for (int i = 0; i < computers.size(); i++) {
           for (int j = i + 1; j < computers.size(); j++) {
               if (computers.get(i).getName().compareTo(computers.get(j).getName()) > 0) {
                   Collections.swap(computers, i, j);
               }
               else if (computers.get(i).getName().compareTo(computers.get(j).getName()) == 0) {
                   if (computers.get(i).getId() > computers.get(j).getId()) {
                       Collections.swap(computers, i, j);
                   }
               }
           }
       }
    }
    @Override
    public boolean checkIDExisted(int id){
        ArrayList<Computer> computers = computerDao.displayAllComputer();
        for(Computer computer : computers){
            if (computer.getId() == id){
                return true;
            }
        }
        return false;
    }
}
