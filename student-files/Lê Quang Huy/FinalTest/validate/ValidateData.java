package validate;

import model.Computer;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class ValidateData {
    private static final Set<String> VALID_COLORS = new HashSet<>();
    static {
        VALID_COLORS.add("red");
        VALID_COLORS.add("blue");
        VALID_COLORS.add("green");
        VALID_COLORS.add("yellow");
        VALID_COLORS.add("black");
        VALID_COLORS.add("white");
        VALID_COLORS.add("purple");
        VALID_COLORS.add("orange");
        VALID_COLORS.add("pink");
    } //(static initializer block)
    public static boolean validateId(int id, ArrayList<Computer> computers){
        for(Computer computer : computers){
            if (computer == null){
                return true;
            }
            if(computer.getId() == id){
                return false;
            }
        }
        return true;
    }
    public static boolean validateName(String name){
        return name.length() >= 5;
    }
    public static boolean validateRam(byte ram){
        return ram >= 2 && ram <= 128;
    }
    public static boolean validateCpu(float cpu){return cpu >= 1 && cpu <= 10;}
    public static boolean validateHdd(short hdd){return hdd >= 128;}
    public static boolean validateWeight(float weight){
        return weight >= 0.5 && weight <= 8;
    }
    public static boolean validateColor(String color) {return VALID_COLORS.contains(color.toLowerCase());}
}
