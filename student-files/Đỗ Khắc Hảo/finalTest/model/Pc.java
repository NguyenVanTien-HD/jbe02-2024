package finalTest.model;

public class Pc extends Computer{

    public Pc(int id, String name, byte ram, float cpu, short hdd, ComputerType type) {
        super(id, name, ram, cpu, hdd, type);
    }
}
