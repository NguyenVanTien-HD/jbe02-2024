package finalTest.model;

public class Computer {
    private int id;
    private String name;
    private byte ram;
    private float cpu;
    private short hdd;
    // TODO: cái này ok nha, nhưng chưa sử dụng triệt để trong bài
    private ComputerType type;

    public Computer(int id, String name, byte ram, float cpu, short hdd) {
        this.id = id;
        this.name = name;
        this.ram = ram;
        this.cpu = cpu;
        this.hdd = hdd;
    }

    public Computer(int id, String name, byte ram, float cpu, short hdd, ComputerType type) {
        this.id = id;
        this.name = name;
        this.ram = ram;
        this.cpu = cpu;
        this.hdd = hdd;
        this.type = type;
    }

    public ComputerType getType() {
        return type;
    }

    public void setType(ComputerType type) {
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte getRam() {
        return ram;
    }

    public void setRam(byte ram) {
        this.ram = ram;
    }

    public float getCpu() {
        return cpu;
    }

    public void setCpu(float cpu) {
        this.cpu = cpu;
    }

    public short getHdd() {
        return hdd;
    }

    public void setHdd(short hdd) {
        this.hdd = hdd;
    }

    @Override
    public String toString() {
        return "Computer{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", ram=" + ram +
                ", cpu=" + cpu +
                ", hdd=" + hdd +
                ", type=" + type +
                '}';
    }
}
