package finalTest.service;

import finalTest.client.DeviceForm;
import finalTest.dao.DeviceDao;
import finalTest.model.Computer;

import java.util.List;

public class DeviceServiceImpl implements DeviceService {
    private DeviceDao deviceDao;
    private DeviceForm deviceForm;

    public DeviceServiceImpl(DeviceDao deviceDao, DeviceForm deviceForm) {
        this.deviceDao = deviceDao;
        this.deviceForm = deviceForm;
    }

    @Override
    public void addDevice() {
            Computer device = deviceForm.getDevice();
            this.deviceDao.addDevice(device);
    }
    @Override
    public void update() {
        // TODO: kiểm tra trùng id để làm gì trong khi mình cần cập nhật. Tính năng này em test chưa vậy?
        // trong khi phần thêm mới không kiểm tra
        int id = this.deviceForm.getId();
        Computer computer = this.deviceForm.getDevice();
        this.deviceDao.update(id, computer);
    }

    @Override
    public void removeDevice() {
        // TODO: kiểm tra trùng id để làm gì trong khi mình cần cập nhật. Tính năng này em test chưa vậy?
        int id = this.deviceForm.getId();
        this.deviceDao.removeDevice(id);
    }

    @Override
    public List<Computer> display() {
        // Todo: chưa sắp xếp
        for (Computer computer : this.deviceDao.display()) {
            System.out.println(computer);
        }
        // Todo: trả về để làm gì?
        return this.deviceDao.display();
    }

    @Override
    public List<Computer> findDeviceByName(String nameSearch) {
        // Todo: Chưa kiểm tra trường hợp không tìm thấy device nào nhỉ
        for (Computer computer : this.deviceDao.findDeviceByName(nameSearch)) {
            System.out.println(computer);
        }
        // Todo: trả về để làm gì?
        return this.deviceDao.findDeviceByName(nameSearch);
    }
}
