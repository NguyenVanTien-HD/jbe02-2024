package finalTest.service;

import finalTest.model.Computer;

import java.util.List;

public interface DeviceService {
    void addDevice();
    void update();
    void removeDevice();
    List<Computer> display();
    List<Computer> findDeviceByName(String nameSearch);
}
