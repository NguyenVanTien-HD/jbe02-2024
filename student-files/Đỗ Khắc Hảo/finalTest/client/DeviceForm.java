package finalTest.client;

import finalTest.exception.DeviceException;
import finalTest.model.Computer;
import finalTest.model.ComputerType;
import finalTest.model.Laptop;
import finalTest.model.Pc;
import finalTest.utils.Validator;

import java.util.ArrayList;
import java.util.Scanner;

public class DeviceForm {
    private Scanner sc;
    private ArrayList<Computer> computers = new ArrayList<>();

    public DeviceForm() {
        this.sc = new Scanner(System.in);
    }

    public int getId() {
        int id;
        System.out.print("Enter device id: ");
        id = sc.nextInt();
        sc.nextLine();
        if (Validator.isDulicatedId(id, computers)) {
            throw new DeviceException("The device id " + id + " is duplicated");
        }
        return id;
    }

    public Computer getDevice(){
        // Todo: ném lỗi ra nhưng bắt ở đâu vay?

        System.out.print("Enter device type (1 for Computer, 2 for Laptop, 3 for Pc): ");
        int type = sc.nextInt();
        sc.nextLine();

        System.out.print("Enter name: ");
        String name = sc.nextLine();
        if (!Validator.validateName(name)) {
            throw new DeviceException("The device name " + name + " is invalid");
        }

        System.out.print("Enter Ram: ");
        byte ram = sc.nextByte();
        if (!Validator.validateRam(ram)) {
            throw new DeviceException("The device ram " + ram + " is invalid");
        }

        System.out.print("Enter Cpu: ");
        float cpu = sc.nextFloat();
        if (!Validator.validateCpu(cpu)) {
            throw new DeviceException("The device cpu " + cpu + " is invalid");
        }

        System.out.print("Enter Hdd: ");
        short hdd = sc.nextShort();
        if (!Validator.validateHdd(hdd)) {
            throw new DeviceException("The device hdd " + hdd + " is invalid");
        }

        if (type == 2) {
            System.out.print("Enter weight: ");
            float weight = sc.nextFloat();
            if (!Validator.validateWeight(weight)) {
                throw new DeviceException("The device weight " + weight + " is invalid");
            }

            sc.nextLine();
            System.out.print("Enter color: ");
            String color = sc.nextLine();
            if (!Validator.validateColor(color)) {
                throw new DeviceException("The device color " + color + " is invalid");
            }

            return new Laptop(0, name, ram, cpu, hdd, ComputerType.LAPTOP, weight, color);
        } else if (type == 3) {
            return new Pc(0, name, ram, cpu, hdd, ComputerType.PC);
        } else {
            return new Computer(0, name, ram, cpu, hdd);
        }
    }
}
