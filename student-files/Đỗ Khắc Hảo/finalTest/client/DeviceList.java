package finalTest.client;

import finalTest.dao.DeviceDao;
import finalTest.dao.DeviceDaoImpl;
import finalTest.service.DeviceServiceImpl;
import java.util.Scanner;

public class DeviceList {
    private static Scanner scanner = new Scanner(System.in);
    private DeviceDao deviceDao = new DeviceDaoImpl();
    private DeviceForm deviceForm = new DeviceForm();
    private DeviceServiceImpl deviceService;
    public DeviceList() {
        this.deviceService = new DeviceServiceImpl(deviceDao, deviceForm);
    }
    public static void main(String[] args) {
        boolean isRunning = true;
        DeviceList deviceList = new DeviceList();

        while (isRunning) {
            System.out.println("================ Device Management ================");
            System.out.println("1. Create");
            System.out.println("2. Display all");
            System.out.println("3. Update");
            System.out.println("4. Delete");
            System.out.println("5. Search by name");
            System.out.println("6. Quit");
            System.out.print("Enter your choice: ");

            int choice = scanner.nextInt();

            switch (choice) {
                case 1:
                    deviceList.deviceService.addDevice();
                    break;
                case 2:
                    deviceList.deviceService.display();
                    break;
                case 3:
                    deviceList.deviceService.update();
                    break;
                case 4:
                    deviceList.deviceService.removeDevice();
                    break;
                case 5:
                    scanner.nextLine();
                    System.out.println("Enter name search: ");
                    String name = scanner.nextLine();
                    deviceList.deviceService.findDeviceByName(name);
                    break;
                case 6:
                    isRunning = false;
                    System.out.println("Exiting...");
                    break;
                default:
                    System.err.println("Invalid choice");
            }
        }
    }
}
