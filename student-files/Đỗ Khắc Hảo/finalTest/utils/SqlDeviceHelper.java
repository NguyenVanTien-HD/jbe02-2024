package finalTest.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class SqlDeviceHelper {
    public static Connection getConnection() throws SQLException {
        String url = "jdbc:mysql://localhost:3306/r2s_device";
        String user = "root";
        String pass = "170804";

        return DriverManager.getConnection(url, user, pass);
    }
}
