package finalTest.utils;

import finalTest.model.Computer;

import java.util.ArrayList;

public class Validator {
    private static final String[] VALID_COLORS = {"red", "white", "blue", "gray"};

    // Todo: constructor private nha em (lý do thì xem lại các bài anh đã nói)
    public Validator() {

    }
    public static boolean isDulicatedId(int id, ArrayList<Computer> computers) {
        for (Computer computer : computers) {
            if (computer.getId() == id) {
                return true;
            }
        }
        return false;
    }
    public static boolean validateName(String name) {
        // Todo: chuyển các số ở đây về constant nhé
        if(name.length() < 5) {
            return false;
        }
        return true;
    }
    public static boolean validateRam(byte ram) {
        // Todo: chuyển các số ở đây về constant nhé
        if(ram >= 2) {
            return true;
        }
        return false;
    }
    public static boolean validateCpu(float cpu) {
        // Todo: chuyển các số ở đây về constant nhé
        if(cpu >= 1 && cpu <= 10) {
            return true;
        }
        return false;
    }
    public static boolean validateHdd(short hdd) {
        // Todo: chuyển các số ở đây về constant nhé
        if(hdd >= 128) {
            return true;
        }
        return false;
    }
    public static boolean validateWeight(float weight) {
        // Todo: chuyển các số ở đây về constant nhé
        if(weight >= 0.5 && weight <= 8) {
            return true;
        }
        return false;
    }
    public static boolean validateColor(String color) {
        // TODO: nên chuyển thành enum rồi dùng .values() ấy em
        for (String validColor : VALID_COLORS) {
            if (validColor.equals(color)) {
                return true;
            }
        }
        return false;
    }
}
