package finalTest.dao;

import finalTest.model.Computer;
import finalTest.model.ComputerType;
import finalTest.model.Laptop;
import finalTest.model.Pc;
import finalTest.utils.SqlDeviceHelper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DeviceDaoImpl implements DeviceDao {

    @Override
    public void addDevice(Computer computer){
        String sqlAdd = "insert into computer(name, ram, cpu, hdd, weight, color, type) VALUES (?, ?, ?, ?, ?, ?, ?)";

        try (Connection conn = SqlDeviceHelper.getConnection();
             PreparedStatement statementComputer = conn.prepareStatement(sqlAdd)) {

            statementComputer.setString(1, computer.getName());
            statementComputer.setByte(2, computer.getRam());
            statementComputer.setFloat(3, computer.getCpu());
            statementComputer.setShort(4, computer.getHdd());

            // truong hop laptop
            if (computer.getType() == ComputerType.LAPTOP) {
                Laptop laptop = (Laptop) computer;
                statementComputer.setFloat(5, laptop.getWeight());
                statementComputer.setString(6, laptop.getColor());
                statementComputer.setString(7, "laptop");
            }else if(computer.getType() == ComputerType.PC) {
                // truong hop pc
                Pc pc = (Pc) computer;
                statementComputer.setNull(5, java.sql.Types.FLOAT);
                statementComputer.setNull(6, java.sql.Types.VARCHAR);
                statementComputer.setString(7, "pc");
            }else {
                statementComputer.setString(5, "computer");
            }

            int rowAffected = statementComputer.executeUpdate();
            System.out.println("So computer them thanh cong: " + rowAffected);


        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void update(int id, Computer computer) {
        String sqlUpdate = "update computer set name = ?, ram = ?, cpu = ?, hdd = ?, type = ? where id = ?";

        String sqlComputer = "UPDATE computer SET name = ?, ram = ?, cpu = ?, hdd = ?, type = ?, weight = ?, color = ? WHERE id = ?";

        try (Connection conn = SqlDeviceHelper.getConnection();
             PreparedStatement statementComputer = conn.prepareStatement(sqlComputer)) {

            statementComputer.setString(1, computer.getName());
            statementComputer.setByte(2, computer.getRam());
            statementComputer.setFloat(3, computer.getCpu());
            statementComputer.setShort(4, computer.getHdd());
            statementComputer.setString(5, computer.getType().toString());

            if (computer.getType() == ComputerType.LAPTOP) {
                Laptop laptop = (Laptop) computer;
                statementComputer.setFloat(6, laptop.getWeight());
                statementComputer.setString(7, laptop.getColor());
            } else {
                statementComputer.setNull(6, java.sql.Types.FLOAT);
                statementComputer.setNull(7, java.sql.Types.VARCHAR);
            }

            statementComputer.setInt(8, id);

            int rowAffected = statementComputer.executeUpdate();
            System.out.println("So computer cap nhat thanh cong: " + rowAffected);


        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void removeDevice(int id){
        String sqlDelete = "delete from computer where id = ?";

        try (Connection conn = SqlDeviceHelper.getConnection();
             PreparedStatement statementComputer = conn.prepareStatement(sqlDelete)) {

            statementComputer.setInt(1, id);
            int rowAffected = statementComputer.executeUpdate();
            System.out.println("So computer xoa thanh cong: " + rowAffected);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Computer> display() {
        List<Computer> result = new ArrayList<>();
        String sqlSelectComputer = "select * from computer";

        try (Connection conn = SqlDeviceHelper.getConnection();
             PreparedStatement statementComputer = conn.prepareStatement(sqlSelectComputer);
             ResultSet resultSetComputer = statementComputer.executeQuery()) {

            while (resultSetComputer.next()) {
                int id = resultSetComputer.getInt("id");
                String name = resultSetComputer.getString("name");
                byte ram = resultSetComputer.getByte("ram");
                float cpu = resultSetComputer.getFloat("cpu");
                short hdd = resultSetComputer.getShort("hdd");
                String typeStr = resultSetComputer.getString("type").toUpperCase();
                ComputerType type = ComputerType.valueOf(typeStr);

                if (type == ComputerType.LAPTOP) {
                    float weight = resultSetComputer.getFloat("weight");
                    String color = resultSetComputer.getString("color");
                    result.add(new Laptop(id, name, ram, cpu, hdd, type, weight, color));
                } else if(type == ComputerType.PC){
                    result.add(new Pc(id, name, ram, cpu, hdd, type));
                } else {
                    result.add(new Computer(id, name, ram, cpu, hdd, type));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return result;
    }
    public List<Computer> findDeviceByName(String nameSearch){
        List<Computer> devices = new ArrayList<>();
        String sql = "select * from computer where name like ?";

        try (Connection conn = SqlDeviceHelper.getConnection();
             PreparedStatement statement = conn.prepareStatement(sql)) {

            statement.setString(1, "%" + nameSearch + "%");
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    int id = resultSet.getInt("id");
                    String name = resultSet.getString("name");
                    byte ram = resultSet.getByte("ram");
                    float cpu = resultSet.getFloat("cpu");
                    short hdd = resultSet.getShort("hdd");
                    String typeStr = resultSet.getString("type").toUpperCase();
                    ComputerType type = ComputerType.valueOf(typeStr);

                    if (type == ComputerType.LAPTOP) {
                        float weight = resultSet.getFloat("weight");
                        String color = resultSet.getString("color");
                        devices.add(new Laptop(id, name, ram, cpu, hdd, type, weight, color));
                    } else if (type == ComputerType.PC) {
                        devices.add(new Pc(id, name, ram, cpu, hdd, type));
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return devices;
    }
}
