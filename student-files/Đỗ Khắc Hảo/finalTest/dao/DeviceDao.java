package finalTest.dao;

import finalTest.model.Computer;
import java.util.List;

public interface DeviceDao {
    void addDevice(Computer computer);
    void update(int id, Computer computer);
    void removeDevice(int id);
    List<Computer> display();
    List<Computer> findDeviceByName(String nameSearch);
}
