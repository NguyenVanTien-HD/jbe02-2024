package FinalTest.service;

import FinalTest.model.Laptop;

import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

public interface DeviceService {
    void add(Laptop laptop);
    List<Laptop> display() throws SQLException;
    void update(Scanner scanner) throws SQLException;
    void remove(Scanner scanner) throws SQLException;
    List<Laptop> find(Scanner scanner);
}
