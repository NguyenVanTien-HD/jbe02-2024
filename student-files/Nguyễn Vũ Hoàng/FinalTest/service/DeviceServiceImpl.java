package FinalTest.service;

import FinalTest.model.Laptop;
import FinalTest.utils.SqlHelper;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

// TODO: nên tách ra thành lớp DAO nhé
// TODO: bài này anh đang thấy em quản lý laptop thì đúng hơn, chưa rõ sự xuất hiện của PC
public class DeviceServiceImpl implements DeviceService {
    List<Laptop> devices = new ArrayList<>();
    private static Set<Integer> usedIds = new HashSet<>();
    private static Scanner scanner = new Scanner(System.in);
    @Override
    public void add(Laptop laptop) {
        // insert data to database
        String sql = "insert into laptop(id, name, ram, cpu, hdd, weight, color) values(?,?,?,?,?,?,?)";
        try (Connection conn = SqlHelper.getConnection();
             PreparedStatement statement = conn.prepareStatement(sql);
        ){
            if (conn != null) {
//                System.out.println("Successful");

                statement.setInt(1,laptop.getId());
                statement.setString(2, laptop.getName());
                statement.setByte(3, laptop.getRam());
                statement.setFloat(4, laptop.getCpu());
                statement.setShort(5, laptop.getHdd());
                statement.setFloat(6, laptop.getWeight());
                statement.setString(7, laptop.getColor());
                //excute
                int rowAffected = statement.executeUpdate();
                System.out.println("Them thanh cong " + rowAffected + " thiet bi");
            } else {
                    System.out.println("Unsuccessful");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Laptop> display() {
        Connection conn =  null;
        try {
            conn = SqlHelper.getConnection();
            if (conn != null) {
                PreparedStatement statement = conn.prepareStatement("select * from laptop order by name desc, id asc");
                //excute
                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()){
                    // lay bang ghi tra ve dong goi vao doi tuong student
                    int id = resultSet.getInt("id");
                    String name = resultSet.getString("name");
                    byte ram = resultSet.getByte("ram");
                    float cpu = resultSet.getInt("cpu");
                    short hdd = resultSet.getShort("hdd");
                    float weight = resultSet.getFloat("weight");
                    String color = resultSet.getString("color");

                    Laptop laptop = new Laptop(id, name,  ram, cpu, hdd, weight, color);
                    //them vao ds
                    devices.add(laptop);
                }
                if (!devices.isEmpty()) {
                    System.out.println("====================== List Device =======================");
                    // TODO: chưa có sắp xếp nhé
                    for (Laptop laptop : devices) {
                        System.out.println(laptop);
                    }
                } else {
                    System.out.println("Not found device!");
                }
            } else {
                System.out.println("Unsuccessful");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return devices;
    }

    @Override
    public void update(Scanner scanner) throws SQLException {
        System.out.print("Enter ID for update: ");
        int id = scanner.nextInt();
        scanner.nextLine();

        Laptop laptop = findDeviceById(id);
        if (laptop == null) {
            System.out.println("Device not exist!");
            return;
        }

        // TODO: mấy thông tin này cũng cần validate lại chứ đúng không
        System.out.print("New name : ");
        String newName = scanner.nextLine();

        System.out.print("New ram: ");
        byte newRam = scanner.nextByte();
        scanner.nextLine();

        System.out.print("New number of CPU: ");
        float newCpu = scanner.nextFloat();
        scanner.nextLine();

        System.out.print("New capacity: ");
        short newHdd = scanner.nextShort();
        scanner.nextLine();

        System.out.print("New weight: ");
        float newWeight = scanner.nextFloat();
        scanner.nextLine();

        System.out.print("New color: ");
        String newColor = scanner.nextLine();

        // update device
        laptop.setName(newName.isEmpty() ? laptop.getName() : newName);
        laptop.setRam(newRam != 0 ? newRam : laptop.getRam());
        laptop.setCpu(newCpu != 0 ? newCpu : laptop.getCpu());
        laptop.setHdd(newHdd != 0 ? newHdd : laptop.getHdd());
        laptop.setWeight(newWeight != 0 ? newWeight : laptop.getWeight());
        laptop.setColor(newColor.isEmpty() ? laptop.getColor() : newColor);

        try (Connection conn = SqlHelper.getConnection()) {
            if (conn != null) {
                String sql = "UPDATE laptop SET name=?, ram=?, cpu=?, hdd=?, weight=?, color=? WHERE id=?";
                PreparedStatement statement = conn.prepareStatement(sql);
                statement.setString(1, laptop.getName());
                statement.setByte(2, laptop.getRam());
                statement.setFloat(3, laptop.getCpu());
                statement.setShort(4, laptop.getHdd());
                statement.setFloat(5, laptop.getWeight());
                statement.setString(6, laptop.getColor());
                statement.setInt(7, laptop.getId());

                int rowsAffected = statement.executeUpdate();
                if (rowsAffected > 0) {
                    System.out.println("Successful!");
                } else {
                    System.out.println("Unsuccessful!");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void remove(Scanner scanner) throws SQLException {
        System.out.print("Enter id for remove: ");
        int id = scanner.nextInt();
        scanner.nextLine();

        Laptop laptop = findDeviceById(id);
        if (laptop == null) {
            System.out.println("Device not exist!");
            return;
        }

        System.out.print("Remove device? (y/n): ");
        String confirmation = scanner.nextLine();

        if (confirmation.equalsIgnoreCase("y")) {
            try (Connection conn = SqlHelper.getConnection()) {
                if (conn != null) {
                    String sql = "DELETE FROM laptop WHERE id=?";
                    PreparedStatement statement = conn.prepareStatement(sql);
                    statement.setInt(1, id);

                    int rowsAffected = statement.executeUpdate();
                    if (rowsAffected > 0) {
                        System.out.println("Successfull!");
                    } else {
                        System.out.println("Unsuccesfull!");
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public List<Laptop> find(Scanner scanner)  {
        System.out.print("Enter a character of name: ");
        String searchTerm = scanner.nextLine().toLowerCase();

        try (Connection conn = SqlHelper.getConnection()) {
            if (conn != null) {
                String sql = "SELECT * FROM laptop WHERE lower(name) LIKE ?";
                PreparedStatement statement = conn.prepareStatement(sql);
                statement.setString(1, "%" + searchTerm + "%");

                ResultSet resultSet = statement.executeQuery();
                List<Laptop> result = new ArrayList<>();
                while (resultSet.next()) {
                    int id = resultSet.getInt("id");
                    String name = resultSet.getString("name");
                    byte ram = resultSet.getByte("ram");
                    float cpu = resultSet.getFloat("cpu");
                    short hdd = resultSet.getShort("hdd");
                    float weight = resultSet.getFloat("weight");
                    String color = resultSet.getString("color");

                    result.add(new Laptop(id, name, ram, cpu, hdd, weight, color));
                }

                if (result.isEmpty()) {
                    System.out.println("No devices matching!");
                } else {
                    System.out.println("Result:");
                    // TODO: thiếu sắp xếp theo tên nè
                    for (Laptop laptop : result) {
                        System.out.println(laptop);
                    }
                }

                return devices;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    //check id
    private static boolean isIdUnique(int id){
        return !usedIds.contains(id);
    }


    //check name
    private static boolean isValidName(String name) {
        // TODO: cho các số này vào constant
        return name != null && name.length() >= 5;
    }
    //check ram
    private static boolean isValidRam(byte ram) {
        // TODO: cho các số này vào constant
        return ram >= 2 && ram <= 128;
    }
    //check cpu
    private static boolean isValidCpu(float cpu) {
        // TODO: cho các số này vào constant
        return cpu >= 1 && cpu <= 10;
    }
    //check hdd
    private static boolean isValidHdd(short hdd) {
        // TODO: cho các số này vào constant
        return hdd >= 128;
    }
    //check weight
    private static boolean isValidWeight(float weight) {
        // TODO: cho các số này vào constant
        return weight >= 0.5 && weight <= 8;
    }
    //check color
    private static boolean isValidColor(String color) {
        // TODO: nen cho ds này vào enum
        return color != null && (color.equalsIgnoreCase("red") || color.equalsIgnoreCase("white") || color.equalsIgnoreCase("blue") || color.equalsIgnoreCase("gray"));
    }
    public static Laptop inputLaptop(){
        System.out.println("Enter Laptop Information:");
        System.out.print("Enter ID: ");
        int id = scanner.nextInt();
        scanner.nextLine();
        System.out.print("Enter Name: ");
        String name = scanner.nextLine();
        System.out.print("Enter RAM: ");
        byte ram = scanner.nextByte();
        System.out.print("Enter CPU: ");
        float cpu = scanner.nextFloat();
        System.out.print("Enter HDD: ");
        short hdd = scanner.nextShort();
        System.out.print("Enter Weight: ");
        float weight = scanner.nextFloat();
        scanner.nextLine();
        System.out.print("Enter Color: ");
        String color = scanner.nextLine();
        // check input
        if (isIdUnique(id) || !isValidName(name) || !isValidRam(ram) || !isValidCpu(cpu) || !isValidHdd(hdd) || !isValidWeight(weight) || !isValidColor(color)) {
            // TODO: anh chưa thấy đoạn xử lý lỗi này
            // TODO: giống như việc em nhặt rác em ném đi, không may vào người đi đường là toi
            throw new IllegalArgumentException("Invalid input. Please try again.");
        }
        return new Laptop(id, name, ram, cpu, hdd, weight,color);
    }
    public Laptop findDeviceById(int id) throws SQLException {
        try (Connection conn = SqlHelper.getConnection()) {
            if (conn != null) {
                String sql = "SELECT * FROM laptop WHERE id=?";
                PreparedStatement statement = conn.prepareStatement(sql);
                statement.setInt(1, id);

                ResultSet resultSet = statement.executeQuery();
                if (resultSet.next()) {
                    int laptopId = resultSet.getInt("id");
                    String name = resultSet.getString("name");
                    byte ram = resultSet.getByte("ram");
                    float cpu = resultSet.getFloat("cpu");
                    short hdd = resultSet.getShort("hdd");
                    float weight = resultSet.getFloat("weight");
                    String color = resultSet.getString("color");

                    return new Laptop(laptopId, name, ram, cpu, hdd, weight, color);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

}
