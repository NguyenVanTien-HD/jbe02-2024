package FinalTest;
import FinalTest.model.Laptop;
import FinalTest.service.DeviceService;
import FinalTest.service.DeviceServiceImpl;

import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

public class DeviceList {
    private static int displayMenuAndGetChoice(Scanner scanner) {
        System.out.println("Menu");
        System.out.println("======== Device Management========");
        System.out.println("1. Create");
        System.out.println("2. Display all");
        System.out.println("3. Update");
        System.out.println("4. Delete");
        System.out.println("5. Search by name");
        System.out.println("6. Save to database");
        System.out.println("7. Quit");
        System.out.print("Please choose function you'd like to do: ");
        int choice = scanner.nextInt();
        scanner.nextLine(); // clear the buffer
        return choice;
    }
    public static void main(String[] args) throws SQLException {
        int choice;
        Scanner scanner = new Scanner(System.in);
        DeviceService deviceService = new DeviceServiceImpl();
        boolean running = true;
        while(running){
            choice = displayMenuAndGetChoice(scanner );
            switch (choice){
                case 1:
                    Laptop newLaptop = DeviceServiceImpl.inputLaptop();
                    deviceService.add(newLaptop);
                    break;
                case 2:
                    // TODO: nếu không cần giá trị trả về thì xóa đi nhé. mà "display" thì return về làm gì đâu
                    List<Laptop> devices = deviceService.display();
                    break;
                case 3:
                    deviceService.update(scanner);
                    break;
                case 4:
                    deviceService.remove(scanner);
                    break;
                case 5:
                    // TODO: em để ý cách mình đặt tên cũng như xử lý method:
                    //  find(tìm kiếm gì đó) thì sẽ cần trả về dữ liệu.
                    //  Em vừa cho display luôn trong find. nhớ nguyên tắc S trong SOLID
                    //  Nguyên tắc S áp dụng cả cho method lẫn class luôn, phần service của em cũng thế
                    deviceService.find(scanner);
                    break;
                case 6:
                    break;
                case 7:
                    running = false;
                    System.out.println("Quit !!!");
                    break;
            }
        }

    }
}

