package Assignment.utils;

import Assignment.model.Computer;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

public class Validator {
    private static final Set<Integer> usedIds = new HashSet<>();
    //name must be at least 5 characters
    private static final String NAME_REGEX = "^(?=\\S)(?!.*\\s{2})[\\w\\s]{5,}$";
    //ram is an integer number and ranges from 2 to 128
    private static final Pattern RAM_PATTERN = Pattern.compile("^(?:[2-9]|[1-9][0-9]|1[01][0-9]|128)$");
    //cpu is number and ranges from 1.0 to 10.0
    private static final Pattern CPU_PATTERN = Pattern.compile("^([1-9](\\.\\d+)?|10(\\.0+)?)$");
    //hdd is an integer number and ranges from 128
    private static final Pattern HDD_PATTERN = Pattern.compile("^(?:[1-9]|[1-9][0-9]*)$");
    //weight is number and ranges from 0.5 to 8
    private static final Pattern WEIGHT_PATTERN = Pattern.compile("^(?:0*(?:[0-7](?:\\.\\d+)?|8(?:\\.0+)?))$");
    //color is a valid color representation as red, white, blue, gray
    private static final String COLOR_REGEX = "^(red|white|blue|gray)$";

    public static boolean isUniqueId(int id) {
        if (usedIds.contains(id)) {
            return false;
        }
        usedIds.add(id);
        return true;
    }

    public static boolean validateName(String name, List<Computer> devices){
        if (name.matches(NAME_REGEX)){
            for (Computer device : devices){
                if (device.getName().equals(name)){
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    public static boolean validateRam(byte ram){
        return RAM_PATTERN.matcher(String.valueOf(ram)).matches();
    }

    public static boolean validateCpu(float cpu){
        return CPU_PATTERN.matcher(String.valueOf(cpu)).matches();
    }

    public static boolean validateHdd(short hdd){
        return HDD_PATTERN.matcher(String.valueOf(hdd)).matches();
    }

    public static boolean validateWeight(float weight){
        return WEIGHT_PATTERN.matcher(String.valueOf(weight)).matches();
    }

    public static boolean validateColor(String color){
        return color.matches(COLOR_REGEX);
    }
}
