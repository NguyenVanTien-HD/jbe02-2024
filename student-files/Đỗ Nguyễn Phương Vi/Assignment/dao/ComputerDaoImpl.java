package Assignment.dao;

import Assignment.model.Computer;
import Assignment.model.Laptop;
import Assignment.model.PC;

import Assignment.utils.SqlHelper;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ComputerDaoImpl implements ComputerDao {
    private List<Computer> devices = new ArrayList<>();

    public void createDevice(Computer device) {
        devices.add(device);
    }

    @Override
    public List<Computer> getAllDevices() {
        return devices;
    }

    @Override
    public boolean updateDevice(int id, Computer updatedDevice) {
        for (Computer device : devices) {
            if (device.getId() == id) {
                device.setName(updatedDevice.getName());
                device.setRam(updatedDevice.getRam());
                device.setCpu(updatedDevice.getCpu());
                device.setHdd(updatedDevice.getHdd());
                switch (device.getClass().getSimpleName()) {
                    case "Laptop":
                        if (updatedDevice instanceof Laptop) {
                            ((Laptop) device).setWeight(((Laptop) updatedDevice).getWeight());
                            ((Laptop) device).setColor(((Laptop) updatedDevice).getColor());
                        }
                        break;
                    case "PC":
                        break;
                    default:
                        break;
                }
                return true;
            }
        }
        return false;
    }

    @Override
    public void saveToDatabase() {
        // Todo: em chỉ cần tạo 1 bảng chung thoi
        // Tạo thêm 1 bảng laptop có 4 cột: id, weight, color, computer_id
        String sqlComputer = "INSERT INTO computer (id, name, ram, cpu, hdd) VALUES (?,?,?,?,?)";
        String sqlLaptop = "INSERT INTO laptop (id, name, ram, cpu, hdd, weight, color) VALUES (?,?,?,?,?,?,?)";
        String sqlPC = "INSERT INTO pc (id, name, ram, cpu, hdd) VALUES (?,?,?,?,?)";

        try (Connection conn = SqlHelper.getConnection();
             PreparedStatement statementComputer = conn.prepareStatement(sqlComputer);
             PreparedStatement statementLaptop = conn.prepareStatement(sqlLaptop);
             PreparedStatement statementPC = conn.prepareStatement(sqlPC)) {

            for (Computer device : devices) {
                if (device instanceof Laptop) {
                    Laptop laptop = (Laptop) device;
                    statementLaptop.setInt(1, laptop.getId());
                    statementLaptop.setString(2, laptop.getName());
                    statementLaptop.setByte(3, laptop.getRam());
                    statementLaptop.setFloat(4, laptop.getCpu());
                    statementLaptop.setShort(5, laptop.getHdd());
                    statementLaptop.setFloat(6, laptop.getWeight());
                    statementLaptop.setString(7, laptop.getColor());
                    statementLaptop.addBatch();
                } else if (device instanceof PC) {
                    PC pc = (PC) device;
                    statementPC.setInt(1, pc.getId());
                    statementPC.setString(2, pc.getName());
                    statementPC.setByte(3, pc.getRam());
                    statementPC.setFloat(4, pc.getCpu());
                    statementPC.setShort(5, pc.getHdd());
                    statementPC.addBatch();
                } else {
                    statementComputer.setInt(1, device.getId());
                    statementComputer.setString(2, device.getName());
                    statementComputer.setByte(3, device.getRam());
                    statementComputer.setFloat(4, device.getCpu());
                    statementComputer.setShort(5, device.getHdd());
                    statementComputer.addBatch();
                }
            }
            // Todo: Cái này ok nhé, thêm theo batch giảm được số lan ket nối và tăng performance
            statementComputer.executeBatch();
            statementLaptop.executeBatch();
            statementPC.executeBatch();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Computer> findDevicesByName(String name) {
        // Todo: chưa sắp xếp theo tên nhé
        List<Computer> foundDevices = new ArrayList<>();
        for (Computer device : devices) {
            if (device.getName().toLowerCase().contains(name.toLowerCase())) {
                foundDevices.add(device);
            }
        }
        return foundDevices;
    }


}
