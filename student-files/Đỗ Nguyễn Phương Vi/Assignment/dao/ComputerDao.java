package Assignment.dao;

import Assignment.model.Computer;

import java.util.List;

public interface ComputerDao {
    void createDevice(Computer device);
    List<Computer> getAllDevices();
    boolean updateDevice(int id, Computer updatedDevice);
    void saveToDatabase();
    List<Computer> findDevicesByName(String name);
}
