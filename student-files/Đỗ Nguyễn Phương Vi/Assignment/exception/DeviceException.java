package Assignment.exception;

public class DeviceException extends RuntimeException{
    public DeviceException(String message){
        super(message);
    }
}
