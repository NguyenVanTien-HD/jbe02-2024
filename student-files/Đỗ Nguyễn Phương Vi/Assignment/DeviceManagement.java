package Assignment;

import Assignment.dao.ComputerDao;
import Assignment.dao.ComputerDaoImpl;
import Assignment.exception.DeviceException;
import Assignment.model.Computer;
import Assignment.model.Laptop;
import Assignment.model.PC;
import Assignment.service.DeviceService;
import Assignment.service.DeviceServiceImpl;
import Assignment.utils.Validator;

import java.util.List;
import java.util.Scanner;

public class DeviceManagement {
    private static Scanner scanner = new Scanner(System.in);
    private static DeviceService deviceService;
    private ComputerDao computerDao;

    private DeviceManagement() {
        this.computerDao = new ComputerDaoImpl();
        this.deviceService = new DeviceServiceImpl(computerDao);
    }

    public static void main(String[] args) {
        DeviceManagement deviceManagement = new DeviceManagement();
        boolean running = true;
        while (running){
            try {
                displayMenu();
                String userInput = scanner.nextLine();
                int choice = Integer.parseInt(userInput);
                switch (choice) {
                    case 1:
                        deviceManagement.addDevice();
                        break;
                    case 2:
                        deviceManagement.displayAllDevices();
                        break;
                    case 3:
                        deviceManagement.updateDevice();
                        break;
                    case 4:
                        deviceManagement.findDeviceByName();
                        break;
                    case 5:
                        deviceManagement.saveToDatabase();
                        break;
                    case 6:
                        running = false;
                        System.out.println("Exit.");
                    default:
                        System.out.println("Invalid choice! Please try again.");
                }
            } catch (DeviceException e){
                e.printStackTrace();
            }
        }

    }

    private static void displayMenu() {
        System.out.println("\n Menu");
        System.out.println("1. Create");
        System.out.println("2. Display all");
        System.out.println("3. Update");
        System.out.println("4. Search by name");
        System.out.println("5. Save to database");
        System.out.println("6. Quit");

        System.out.println("Please choose function you'd like to do: ");
    }

    private static void addDevice() {
        System.out.println("Enter device type (1: Computer, 2: Laptop, 3: PC): ");
        int type = scanner.nextInt();
        scanner.nextLine();

        System.out.println("Enter device id: ");
        int id = scanner.nextInt();
        scanner.nextLine();
        if (!Validator.isUniqueId(id)){
            throw new DeviceException("ID is not unique!");
        }

        System.out.println("Enter device name: ");
        String name = scanner.nextLine();
        if (!Validator.validateName(name, deviceService.getAllDevices())){
            throw new DeviceException("Please try again.");
        }

        System.out.println("Enter device RAM: ");
        byte ram = scanner.nextByte();
        scanner.nextLine();
        if (!Validator.validateRam(ram)){
            throw new DeviceException("Please try again.");
        }

        System.out.println("Enter device CPU: ");
        float cpu = scanner.nextFloat();
        scanner.nextLine();
        if (!Validator.validateCpu(cpu)){
            throw new DeviceException("Please try again.");
        }

        System.out.println("Enter device HDD: ");
        short hdd = scanner.nextShort();
        scanner.nextLine();
        if (!Validator.validateHdd(hdd)){
            throw new DeviceException("Please try again.");
        }
        switch (type) {
            case 1:
                Computer computer = new Computer(id, name, ram, cpu, hdd);
                deviceService.createDevice(computer);
                System.out.println("Created successfully!");
                break;
            case 2:
                System.out.println("Enter device weight: ");
                float weight = scanner.nextFloat();
                scanner.nextLine();
                if (!Validator.validateWeight(weight)){
                    throw new DeviceException("Please try again.");
                }

                System.out.println("Enter device color: ");
                String color = scanner.nextLine();
                if (!Validator.validateColor(color)){
                    throw new DeviceException("Please try again.");
                }

                Laptop laptop = new Laptop(id, name, ram, cpu, hdd, weight, color);
                deviceService.createDevice(laptop);
                System.out.println("Created successfully!");
                break;
            case 3:
                PC pc = new PC(id, name, ram, cpu, hdd);
                deviceService.createDevice(pc);
                System.out.println("Created successfully!");
                break;
            default:
                System.out.println("Invalid device type.");
        }
    }

    private static void displayAllDevices() {
        List<Computer> devices = deviceService.getAllDevices();
        if (devices.isEmpty()) {
          throw new DeviceException("No devices found.");
        } else {
            // Todo: bỏ else được đấy và chưa sap xep phần này nhé
            for (Computer device : devices) {
                System.out.println(device);
            }
        }
    }

    private static void updateDevice() {
        System.out.println("Enter device id to update: ");
        int id = scanner.nextInt();
        scanner.nextLine();
        Computer updatedDevice = getUpdatedDeviceDetails();
        if (deviceService.updateDevice(id, updatedDevice)) {
            System.out.println("Device updated successfully.");
        } else {
           throw new DeviceException("Failed to update device.");
        }
    }

    // Todo: em dùng đệ quy cho phần này cũng ok, hoặc có thể dùng throw ex ném ra cũng được
    private static Computer getUpdatedDeviceDetails() {
        System.out.println("Update detail of device.");
        System.out.println("Enter device name: ");
        String name = scanner.nextLine();
        if (!Validator.validateName(name, deviceService.getAllDevices())){
            System.out.println("Please try again.");
            return getUpdatedDeviceDetails();
        }

        System.out.println("Enter device RAM: ");
        byte ram = scanner.nextByte();
        scanner.nextLine();
        if (!Validator.validateRam(ram)){
            System.out.println("Please try again.1");
            return getUpdatedDeviceDetails();
        }

        System.out.println("Enter device CPU: ");
        float cpu = scanner.nextFloat();
        scanner.nextLine();
        if (!Validator.validateCpu(cpu)){
            System.out.println("Please try again");
            return getUpdatedDeviceDetails();
        }

        System.out.println("Enter device HDD: ");
        short hdd = scanner.nextShort();
        scanner.nextLine();
        if (!Validator.validateHdd(hdd)){
            System.out.println("Please try again.");
            return getUpdatedDeviceDetails();
        }

        System.out.println("Enter the type of updated device (1. Computer, 2. Latop, 3. PC)");
        int deviceType = scanner.nextInt();
        scanner.nextLine();
        switch (deviceType){
            case 1:
                return new Computer(name, ram, cpu, hdd);
            case 2:
                System.out.println("Enter device weight:");
                float weight = scanner.nextFloat();
                scanner.nextLine();
                if (!Validator.validateWeight(weight)){
                    System.out.println("Please try again.");
                    return getUpdatedDeviceDetails();
                }

                System.out.println("Enter device color: ");
                String color = scanner.nextLine();
                if (!Validator.validateColor(color)){
                    System.out.println("Please try again.");
                    return getUpdatedDeviceDetails();
                }

                return new Laptop(name, ram, cpu, hdd, weight, color);
            case 3:
                return new PC(name, ram, cpu, hdd);

            default:
                System.out.println("Invalid device type. Please try again.");
                return new Computer(name, ram, cpu,hdd);
            }

        }

    private static void saveToDatabase(){
        deviceService.saveToDatabase();
        System.out.println("Devices saved to the database.");
    }

    private static void findDeviceByName() {
        System.out.println("Enter device name to search: ");
        String name = scanner.nextLine();
        List<Computer> devices = deviceService.findDevicesByName(name);
        if (devices.isEmpty()) {
            throw new DeviceException("No devices found with the given name.");
        } else {
            System.out.println("Devices found:");
            for (Computer device : devices) {
                System.out.println(device);
            }
        }
    }
}
