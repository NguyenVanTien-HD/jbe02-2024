package Assignment.service;

import Assignment.dao.ComputerDao;
import Assignment.model.Computer;

import java.util.List;

public class DeviceServiceImpl implements DeviceService{
    private ComputerDao computerDao;
    public DeviceServiceImpl(ComputerDao computerDao){
        this.computerDao = computerDao;
    }
    @Override
    public void createDevice(Computer device) {
        computerDao.createDevice(device);
    }

    @Override
    public List<Computer> getAllDevices() {
        return computerDao.getAllDevices();
    }

    @Override
    public boolean updateDevice(int id, Computer updatedDevice) {
        return computerDao.updateDevice(id, updatedDevice);
    }

    @Override
    public void saveToDatabase() {
        computerDao.saveToDatabase();
    }


    @Override
    public List<Computer> findDevicesByName(String name) {
        return computerDao.findDevicesByName(name);
    }
}
