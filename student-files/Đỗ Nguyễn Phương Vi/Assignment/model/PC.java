package Assignment.model;

public class PC extends Computer{
    public PC(String name, byte ram, float cpu, short hdd) {
        super(name, ram, cpu, hdd);
    }

    public PC(int id, String name, byte ram, float cpu, short hdd) {
        super(id, name, ram, cpu, hdd);
    }

    @Override
    public String toString() {
        return "PC{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", ram=" + ram +
                ", cpu=" + cpu +
                ", hdd=" + hdd +
                '}';
    }
}
