package Assignment.model;

public class Computer {
    protected int id;
    protected String name;
    protected byte ram;
    protected float cpu;
    protected short hdd;

    public Computer(String name, byte ram, float cpu, short hdd) {
        this.name = name;
        this.ram = ram;
        this.cpu = cpu;
        this.hdd = hdd;
    }

    public Computer(int id, String name, byte ram, float cpu, short hdd) {
        this.id = id;
        this.name = name;
        this.ram = ram;
        this.cpu = cpu;
        this.hdd = hdd;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte getRam() {
        return ram;
    }

    public void setRam(byte ram) {
        this.ram = ram;
    }

    public float getCpu() {
        return cpu;
    }

    public void setCpu(float cpu) {
        this.cpu = cpu;
    }

    public short getHdd() {
        return hdd;
    }

    public void setHdd(short hdd) {
        this.hdd = hdd;
    }

    @Override
    public String toString() {
        return "Computer{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", ram=" + ram +
                ", cpu=" + cpu +
                ", hdd=" + hdd +
                '}';
    }
}
