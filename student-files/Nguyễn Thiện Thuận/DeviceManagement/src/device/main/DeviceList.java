package device.main;

import device.entitie.Computer;
import device.utils.Validator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class DeviceList {
    private List<Computer> computers = new ArrayList<>();

    public void createDevice() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhập ID thiết bị: ");
        String id = scanner.nextLine();

        for (Computer d : computers) {
            if (d.getId().equals(id)) {
                System.out.println("ID bị trùng.");
                return;
            }
        }

        System.out.println("Nhập tên thiết bị: ");
        String name = scanner.nextLine();
        System.out.println("Nhập RAM: ");
        int ram = Integer.parseInt(scanner.nextLine());
        System.out.println("Nhập CPU: ");
        double cpu = Double.parseDouble(scanner.nextLine());
        System.out.println("Nhập HDD: ");
        int hdd = Integer.parseInt(scanner.nextLine());

        Computer computer = new Computer(id, name, ram, cpu, hdd);
        if (Validator.validateDevice(computer)) {
            computers.add(computer);
            System.out.println("Thiết bị được thêm thành công.");
        } else {
            System.out.println("Dữ liệu thiết bị không hợp lệ.");
        }
    }
    public void displayAllDevices() {
        Collections.sort(computers, new Comparator<Computer>() {
            @Override
            public int compare(Computer d1, Computer d2) {
                int nameComparison = d2.getName().compareTo(d1.getName());
                if (nameComparison == 0) {
                    return d1.getId().compareTo(d2.getId());
                }
                return nameComparison;
            }
        });
        for (Computer d : computers) {
            System.out.println(d);
        }
    }

    public static void main(String[] args) {
        DeviceList deviceList = new DeviceList();
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("====== Quản lý Thiết bị ======");
            System.out.println("1. Create ");
            System.out.println("2. Display all ");
            System.out.println("3. Update");
            System.out.println("4. Delete");
            System.out.println("5. Search by name");
            System.out.println("6. Save to database");
            System.out.println("7. Thoát");
            System.out.print("Vui lòng chọn chức năng mà bạn muốn thực hiện: ");
            int choice = scanner.nextInt();
            scanner.nextLine(); // đọc ký tự xuống dòng còn lại

            switch (choice) {
                case 1:
                    deviceList.createDevice();
                    break;
                case 2:
                    deviceList.displayAllDevices();
                    break;
                case 3:
                    // TODO: may cai này cũng đơn giản mà
                    break;
                case 4:
                    // TODO: may cai này cũng đơn giản mà
                    break;
                case 5:
                    // TODO: may cai này cũng đơn giản mà
                    break;
                case 6:
                    // TODO: may cai này cũng đơn giản mà
                    break;
                case 7:
                    System.out.println("Thoát...");
                    System.exit(0);
                default:
                    System.out.println("Lựa chọn không hợp lệ. Vui lòng thử lại.");
            }
        }
    }
}
