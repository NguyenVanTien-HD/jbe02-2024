package device.entitie;

public class Laptop extends Computer {
    private double weight;
    private String color;

    public Laptop(String id, String name, int ram, double cpu, int hdd, double weight, String color) {
        super(id, name, ram, cpu, hdd);
        this.weight = weight;
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
