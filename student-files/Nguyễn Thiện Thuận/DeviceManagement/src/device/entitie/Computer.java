package device.entitie;

public class Computer {
    private String id;
    private String name;
    private int ram;
    private double cpu;
    private int hdd;

    public Computer(String id, String name, int ram, double cpu, int hdd) {
        this.id = id;
        this.name = name;
        this.ram = ram;
        this.cpu = cpu;
        this.hdd = hdd;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getRam() {
        return ram;
    }

    public double getCpu() {
        return cpu;
    }

    public int getHdd() {
        return hdd;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCpu(double cpu) {
        this.cpu = cpu;
    }

    public void setRam(int ram) {
        this.ram = ram;
    }

    public void setHdd(int hdd) {
        this.hdd = hdd;
    }

    @Override
    public String toString() {
        return "Computer{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", ram=" + ram +
                ", cpu=" + cpu +
                ", hdd=" + hdd +
                '}';
    }
}
