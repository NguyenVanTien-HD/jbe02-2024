package device.utils;

import device.entitie.Computer;

// TODO: nhớ có private constructor
public class Validator {
    // TODO: check id trùng chưa đúng nha
    public static boolean isValidId(String id) {
        return id != null && !id.trim().isEmpty();
    }

    // TODO: mấy phan validate này khá ok, em cho các số thành constant nhé
    public static boolean isValidName(String name) {
        return name != null && name.length() >= 5;
    }

    public static boolean isValidRam(int ram) {
        return ram >= 2 && ram <= 128;
    }

    public static boolean isValidCpu(double cpu) {
        return cpu >= 1 && cpu <= 10;
    }

    public static boolean isValidHdd(int hdd) {
        return hdd >= 128;
    }

    public static boolean isValidWeight(double weight) {
        return weight >= 0.5 && weight <= 8;
    }

    public static boolean isValidColor(String color) {
        // TODO: cái này thì cho vào enum
        return color.equals("red") || color.equals("white") || color.equals("blue") || color.equals("gray");
    }

    public static boolean validateDevice(Computer computer) {
        return isValidId(computer.getId()) &&
                isValidName(computer.getName()) &&
                isValidRam(computer.getRam()) &&
                isValidCpu(computer.getCpu()) &&
                isValidHdd(computer.getHdd());

    }
}
