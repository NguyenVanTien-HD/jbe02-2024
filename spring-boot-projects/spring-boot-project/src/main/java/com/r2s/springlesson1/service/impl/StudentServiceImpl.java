package com.r2s.springlesson1.service.impl;

import com.r2s.springlesson1.dto.StudentDto;
import com.r2s.springlesson1.dto.paging.PageDto;
import com.r2s.springlesson1.dto.paging.StudentSearchRequestDto;
import com.r2s.springlesson1.entity.StudentEntity;
import com.r2s.springlesson1.exception.CommonException;
import com.r2s.springlesson1.mapper.StudentMapper;
import com.r2s.springlesson1.repository.StudentCustomRepository;
import com.r2s.springlesson1.repository.StudentRepository;
import com.r2s.springlesson1.service.StudentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class StudentServiceImpl implements StudentService {
    private final StudentRepository studentRepository; // use spring data jpa
    private final StudentMapper studentMapper;
    private final StudentCustomRepository studentCustomRepository;

    public StudentServiceImpl(StudentRepository studentRepository,
                              StudentMapper studentMapper,
                              // TODO: sử dụng Qualifier để chuyển đổi các bean
                              @Qualifier("studentNativeQueryCustomRepository")
                              StudentCustomRepository studentCustomRepository) {
        this.studentRepository = studentRepository;
        this.studentMapper = studentMapper;
        this.studentCustomRepository = studentCustomRepository;
    }


    @Override
    public List<StudentDto> findAll() {
//        List<StudentDto> list = new ArrayList<>();
//
//        List<StudentEntity> entities = studentRepository.findAll();
//        for (StudentEntity entity : entities) {
//            StudentDto dto = studentMapper.toDto(entity);
//            list.add(dto);
//        }
//
//        return list;

        // Stream api
        // Anonymous class
        // lambda expression
        // functional interface
        // Method reference
        return studentRepository.findAll()
                .stream()
                .map(studentMapper::mapped)
                .toList();
    }

    @Override
    public StudentDto findById(Integer id) {
        // try catch
//        StudentEntity entity = studentRepository.findById(id).orElseThrow();
//        StudentDto dto = studentMapper.toDto(entity);
//        return dto;

        return studentRepository.findById(id)
                .map(studentMapper::mapped)
                .orElseThrow(() -> new CommonException("Khong tim thay ...."));
    }

    @Override
    public StudentEntity insert(StudentEntity entity) {
        return studentRepository.save(entity);
    }

    @Override
    public PageDto<StudentDto> search(StudentSearchRequestDto req) {
        List<StudentDto> dtos = studentCustomRepository.search(req.getStudent(), req.getPage(), req.getSize())
                .stream().map(studentMapper::toDto).toList();
        long totalElements = studentCustomRepository.count(req.getStudent());
        long totalPages = (long) Math.ceil((double) totalElements / req.getSize());


        return PageDto.<StudentDto>builder()
                .items(dtos)
                .page(req.getPage())
                .size(req.getSize())
                .totalElements(totalElements)
                .totalPages(totalPages)
                .build();
    }
}
