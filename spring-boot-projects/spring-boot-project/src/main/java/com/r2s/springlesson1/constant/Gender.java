package com.r2s.springlesson1.constant;

import lombok.Getter;

@Getter
public enum Gender {
    MALE(1, "nam"), FEMALE(2, "nữ"),;
    private final int value;
    private final String displayString;


    Gender(int value, String displayString) {
        this.value = value;
        this.displayString = displayString;
    }
}
