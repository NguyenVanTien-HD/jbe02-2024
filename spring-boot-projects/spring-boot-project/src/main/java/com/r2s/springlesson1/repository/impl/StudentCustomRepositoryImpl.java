package com.r2s.springlesson1.repository.impl;

import com.r2s.springlesson1.dto.StudentDto;
import com.r2s.springlesson1.entity.StudentEntity;
import com.r2s.springlesson1.repository.StudentCustomRepository;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.TypedQuery;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("studentCriteriaRepository")
public class StudentCustomRepositoryImpl implements StudentCustomRepository {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<StudentEntity> search(StudentDto studentDto, int page, int size) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<StudentEntity> cq = cb.createQuery(StudentEntity.class);
        Root<StudentEntity> student = cq.from(StudentEntity.class);

        Predicate[] predicates = createPredicates(studentDto, cb, student);
        cq.where(predicates);

        TypedQuery<StudentEntity> query = entityManager.createQuery(cq);
        query.setFirstResult((page - 1) * size);
        query.setMaxResults(size);

        return query.getResultList();
    }

    @Override
    public long count(StudentDto studentDto) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<StudentEntity> student = cq.from(StudentEntity.class);

        Predicate[] predicates = createPredicates(studentDto, cb, student);
        cq.select(cb.count(student)).where(predicates);

        return entityManager.createQuery(cq).getSingleResult();
    }
    private Predicate[] createPredicates(StudentDto studentDto, CriteriaBuilder cb, Root<StudentEntity> student) {
        Predicate p = cb.conjunction();

        if (studentDto.getStudentId() != null) {
            p = cb.and(p, cb.equal(student.get("studentId"), studentDto.getStudentId()));
        }
        if (studentDto.getStudentName() != null) {
            p = cb.and(p, cb.like(student.get("studentName"), "%" + studentDto.getStudentName() + "%"));
        }
        if (studentDto.getAge() != null) {
            p = cb.and(p, cb.equal(student.get("age"), studentDto.getAge()));
        }
        if (studentDto.getGender() != null) {
            p = cb.and(p, cb.equal(student.get("gender"), studentDto.getGender()));
        }
        if (studentDto.getIsPassed() != null) {
            p = cb.and(p, cb.equal(student.get("isPassed"), studentDto.getIsPassed()));
        }

        return new Predicate[]{p};
    }
}
