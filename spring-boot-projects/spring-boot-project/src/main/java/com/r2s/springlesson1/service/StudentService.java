package com.r2s.springlesson1.service;

import com.r2s.springlesson1.dto.StudentDto;
import com.r2s.springlesson1.dto.paging.PageDto;
import com.r2s.springlesson1.dto.paging.StudentSearchRequestDto;
import com.r2s.springlesson1.entity.StudentEntity;

import java.util.List;

public interface StudentService {
    List<StudentDto> findAll();

    StudentDto findById(Integer id);

    StudentEntity insert(StudentEntity entity);

    PageDto<StudentDto> search(StudentSearchRequestDto requestDto);
}
