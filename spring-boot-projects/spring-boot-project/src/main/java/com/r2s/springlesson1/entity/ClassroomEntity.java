package com.r2s.springlesson1.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "classroom")
@Getter
@Setter
public class ClassroomEntity {

    // CSDL: getAll classroom 1
    // abcxyz: 2
    // getStudents: csdl student 3
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long classId;

    @Column(name = "name")
    private String name;

    @Column(name = "created_date")
    private Date createdDate;

}
