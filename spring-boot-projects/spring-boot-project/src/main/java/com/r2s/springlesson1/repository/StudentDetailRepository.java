package com.r2s.springlesson1.repository;

import com.r2s.springlesson1.entity.StudentDetailEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentDetailRepository extends JpaRepository<StudentDetailEntity, Long> {
}
