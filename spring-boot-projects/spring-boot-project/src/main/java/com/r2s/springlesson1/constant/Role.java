package com.r2s.springlesson1.constant;

public enum Role {
    USER, ADMIN
}
