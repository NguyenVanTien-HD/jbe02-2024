package com.r2s.springlesson1.exception;

public class CommonException extends RuntimeException {
    public CommonException(String message) {
        super(message);
    }
}
