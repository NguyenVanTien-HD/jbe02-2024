package com.r2s.springlesson1.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.*;

@Entity
@Table(name = "student")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder // 1. creator pattern
public class StudentEntity {
    @Id
    @Column(name = "student_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer studentId;

    @Column(name = "student_name")
    @NotBlank
    // Blank: null, "", "       "
    // Empty: null, ""
    @Size(max = 10)
    private String studentName;

    @Column(name = "age")
    @Min(10)
    @Max(value = 100, message = "must be less than or equal to 100")
    private Integer age;

    @Column(name = "gender")
    private Integer gender;
    // Nam Nữ

    @Column(name = "is_passed")
    private Boolean isPassed;

    // N student - 1 classroom
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "class_id")
    @JsonIgnore
    private ClassroomEntity classroom;

    // 1 student - 1 student detail
    @OneToOne(mappedBy = "student", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JsonIgnore
    private StudentDetailEntity studentDetail;
    // Khi lấy dữ lịệu student. dữ liệu student detail load ra cùng.
}
