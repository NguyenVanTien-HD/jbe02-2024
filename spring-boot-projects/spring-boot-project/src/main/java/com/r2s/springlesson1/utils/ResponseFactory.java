package com.r2s.springlesson1.utils;

import com.r2s.springlesson1.constant.AppConstant;
import com.r2s.springlesson1.dto.common.BaseResponse;
import org.springframework.http.ResponseEntity;

public class ResponseFactory {
    private ResponseFactory() {}

    public static <T> ResponseEntity<BaseResponse<T>> ok(T data) {
        BaseResponse<T> response = new BaseResponse<>();
        response.setData(data);
        response.setStatus(AppConstant.SUCCESS_CODE);
        response.setDescription("success");

        return ResponseEntity.ok(response);
    }

    public static <T> ResponseEntity<BaseResponse<T>> error(T data, String errorCode, String msg) {
        BaseResponse<T> response = new BaseResponse<>();
        response.setData(data);
        response.setStatus(errorCode);
        response.setDescription(msg);

        return ResponseEntity.ok(response);
    }
}
