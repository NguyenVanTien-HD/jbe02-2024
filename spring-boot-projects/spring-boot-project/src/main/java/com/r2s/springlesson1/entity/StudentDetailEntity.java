package com.r2s.springlesson1.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "student_detail")
@Getter
@Setter
public class StudentDetailEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long studentDetailId;

    @Column(name = "passport")
    private String passport;

    @Column(name = "address")
    private String address;

    // 1 student - 1 student detail
    @OneToOne
    @JoinColumn(name = "student_id", nullable = false)
    private StudentEntity student;
}
