package com.r2s.springlesson1.dto;

import lombok.Data;

@Data
public class StudentDto {
    private Integer studentId;
    private String studentName;
    private Integer age;
    private Integer gender;
    private Boolean isPassed;
    private String genderDisplay;
    private String className;
}
