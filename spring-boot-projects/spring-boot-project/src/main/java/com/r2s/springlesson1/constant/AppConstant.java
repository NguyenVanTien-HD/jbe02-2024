package com.r2s.springlesson1.constant;

public class AppConstant {
    private AppConstant() {}

    public static final Integer MALE = 1;
    public static final Integer FEMALE = 0;

    public static final String SUCCESS_CODE = "00";
}
