package com.r2s.springlesson1.service;

import com.r2s.springlesson1.dto.JwtDto;
import com.r2s.springlesson1.dto.UserDTO;

public interface UserService {
    UserDTO register(UserDTO user);
    JwtDto login(UserDTO user);
}
