package com.r2s.springlesson1.dto;

import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO {
    private Long id;
    // nguoi dung co the truyen hoac khong truyen
    private String username;
    private String password;

    @NotEmpty
    private List<String> roles;
}
