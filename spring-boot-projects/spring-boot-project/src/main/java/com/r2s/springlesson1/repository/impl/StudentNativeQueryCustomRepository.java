package com.r2s.springlesson1.repository.impl;

import com.r2s.springlesson1.dto.StudentDto;
import com.r2s.springlesson1.entity.ClassroomEntity;
import com.r2s.springlesson1.entity.StudentEntity;
import com.r2s.springlesson1.repository.StudentCustomRepository;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository("studentNativeQueryCustomRepository")
public class StudentNativeQueryCustomRepository implements StudentCustomRepository {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<StudentEntity> search(StudentDto studentDto, int page, int size) {
        StringBuilder sql = new StringBuilder(
                "SELECT student_id, student_name, age, gender, is_passed, class_id" +
                        " FROM student WHERE 1 = 1");
        List<Object> parameters = new ArrayList<>();

        if (studentDto.getStudentId() != null) {
            sql.append(" AND student_id = ?");
            parameters.add(studentDto.getStudentId());
        }
        if (studentDto.getStudentName() != null) {
            sql.append(" AND student_name LIKE ?");
            parameters.add("%" + studentDto.getStudentName() + "%");
        }
        if (studentDto.getAge() != null) {
            sql.append(" AND age = ?");
            parameters.add(studentDto.getAge());
        }
        if (studentDto.getGender() != null) {
            sql.append(" AND gender = ?");
            parameters.add(studentDto.getGender());
        }
        if (studentDto.getIsPassed() != null) {
            sql.append(" AND is_passed = ?");
            parameters.add(studentDto.getIsPassed());
        }


        sql.append(" LIMIT ? OFFSET ?");
        parameters.add(size);
        parameters.add((page - 1) * size);

        Query query = entityManager.createNativeQuery(sql.toString());

        for (int i = 0; i < parameters.size(); i++) {
            query.setParameter(i + 1, parameters.get(i));
        }

        return convertToStudentList(query.getResultList());
    }

    @Override
    public long count(StudentDto studentDto) {
        StringBuilder sql = new StringBuilder("SELECT COUNT(*) FROM student WHERE 1=1");
        List<Object> parameters = new ArrayList<>();

        if (studentDto.getStudentId() != null) {
            sql.append(" AND student_id = ?");
            parameters.add(studentDto.getStudentId());
        }
        if (studentDto.getStudentName() != null) {
            sql.append(" AND student_name LIKE ?");
            parameters.add("%" + studentDto.getStudentName() + "%");
        }
        if (studentDto.getAge() != null) {
            sql.append(" AND age = ?");
            parameters.add(studentDto.getAge());
        }
        if (studentDto.getGender() != null) {
            sql.append(" AND gender = ?");
            parameters.add(studentDto.getGender());
        }
        if (studentDto.getIsPassed() != null) {
            sql.append(" AND is_passed = ?");
            parameters.add(studentDto.getIsPassed());
        }

        Query query = entityManager.createNativeQuery(sql.toString());

        for (int i = 0; i < parameters.size(); i++) {
            query.setParameter(i + 1, parameters.get(i));
        }

        return ((Number) query.getSingleResult()).longValue();
    }

    public static List<StudentEntity> convertToStudentList(List<Object[]> results) {
        List<StudentEntity> students = new ArrayList<>();
        for (Object[] row : results) {
            StudentEntity student = new StudentEntity();
            student.setStudentId((Integer) row[0]);
            student.setStudentName((String) row[1]);
            student.setAge((Integer) row[2]);
            student.setGender((Integer) row[3]);
            student.setIsPassed((Boolean) row[4]);
            Long classId = (Long) row[5];
            ClassroomEntity classroom = new ClassroomEntity();
            classroom.setClassId(classId);

            student.setClassroom(classroom);

            students.add(student);
        }
        return students;
    }
}
