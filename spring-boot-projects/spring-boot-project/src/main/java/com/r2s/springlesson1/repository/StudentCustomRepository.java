package com.r2s.springlesson1.repository;

import com.r2s.springlesson1.dto.StudentDto;
import com.r2s.springlesson1.entity.StudentEntity;

import java.util.List;

public interface StudentCustomRepository {

    // tim kiem theo nhieu dieu kien
    List<StudentEntity> search(StudentDto studentDto, int page, int pageSize);
    long count(StudentDto studentDto);
}
