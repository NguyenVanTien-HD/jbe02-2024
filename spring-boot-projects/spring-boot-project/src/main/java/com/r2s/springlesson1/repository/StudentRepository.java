package com.r2s.springlesson1.repository;

import com.r2s.springlesson1.entity.StudentEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentRepository extends JpaRepository<StudentEntity, Integer> {


    // JPQL/HQL
    @Query("select a from StudentEntity as a where a.age = :age")
    List<StudentEntity> findAllByAge(@Param("age") int age);

    @Query("from StudentEntity as a where a.studentName = :name")
    List<StudentEntity> findAllByStudentName(@Param("name") String studentName);

    // native query sql
    @Query(nativeQuery = true,
    value = "select * from student where gender = :gender"
    )
    List<StudentEntity> findAllByGender(@Param("gender") int gender);
}
