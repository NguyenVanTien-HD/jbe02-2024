package com.r2s.springlesson1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringLesson1Application {

    public static void main(String[] args) {
//        StudentRepository repository = new StudentRepository() {
//        }


        // IOC: Bean management
//        1/ khoi tao

//        StudentRepository studentRepository = new StudentRepository() {
//            @Override
//            public void flush() {
//
//            }
//
//            @Override
//            public <S extends StudentEntity> S saveAndFlush(S entity) {
//                return null;
//            }
//
//            @Override
//            public <S extends StudentEntity> List<S> saveAllAndFlush(Iterable<S> entities) {
//                return null;
//            }
//
//            @Override
//            public void deleteAllInBatch(Iterable<StudentEntity> entities) {
//
//            }
//
//            @Override
//            public void deleteAllByIdInBatch(Iterable<Integer> integers) {
//
//            }
//
//            @Override
//            public void deleteAllInBatch() {
//
//            }
//
//            @Override
//            public StudentEntity getOne(Integer integer) {
//                return null;
//            }
//
//            @Override
//            public StudentEntity getById(Integer integer) {
//                return null;
//            }
//
//            @Override
//            public StudentEntity getReferenceById(Integer integer) {
//                return null;
//            }
//
//            @Override
//            public <S extends StudentEntity> List<S> findAll(Example<S> example) {
//                return null;
//            }
//
//            @Override
//            public <S extends StudentEntity> List<S> findAll(Example<S> example, Sort sort) {
//                return null;
//            }
//
//            @Override
//            public <S extends StudentEntity> List<S> saveAll(Iterable<S> entities) {
//                return null;
//            }
//
//            @Override
//            public List<StudentEntity> findAll() {
//                return null;
//            }
//
//            @Override
//            public List<StudentEntity> findAllById(Iterable<Integer> integers) {
//                return null;
//            }
//
//            @Override
//            public <S extends StudentEntity> S save(S entity) {
//                return null;
//            }
//
//            @Override
//            public Optional<StudentEntity> findById(Integer integer) {
//                return Optional.empty();
//            }
//
//            @Override
//            public boolean existsById(Integer integer) {
//                return false;
//            }
//
//            @Override
//            public long count() {
//                return 0;
//            }
//
//            @Override
//            public void deleteById(Integer integer) {
//
//            }
//
//            @Override
//            public void delete(StudentEntity entity) {
//
//            }
//
//            @Override
//            public void deleteAllById(Iterable<? extends Integer> integers) {
//
//            }
//
//            @Override
//            public void deleteAll(Iterable<? extends StudentEntity> entities) {
//
//            }
//
//            @Override
//            public void deleteAll() {
//
//            }
//
//            @Override
//            public List<StudentEntity> findAll(Sort sort) {
//                return null;
//            }
//
//            @Override
//            public Page<StudentEntity> findAll(Pageable pageable) {
//                return null;
//            }
//
//            @Override
//            public <S extends StudentEntity> Optional<S> findOne(Example<S> example) {
//                return Optional.empty();
//            }
//
//            @Override
//            public <S extends StudentEntity> Page<S> findAll(Example<S> example, Pageable pageable) {
//                return null;
//            }
//
//            @Override
//            public <S extends StudentEntity> long count(Example<S> example) {
//                return 0;
//            }
//
//            @Override
//            public <S extends StudentEntity> boolean exists(Example<S> example) {
//                return false;
//            }
//
//            @Override
//            public <S extends StudentEntity, R> R findBy(Example<S> example, Function<FluentQuery.FetchableFluentQuery<S>, R> queryFunction) {
//                return null;
//            }
//        };
//
//
////        2. DI
//        StudentController controller = new StudentController(studentRepository);

        // 3 destroy
        SpringApplication.run(SpringLesson1Application.class, args);
    }

}
