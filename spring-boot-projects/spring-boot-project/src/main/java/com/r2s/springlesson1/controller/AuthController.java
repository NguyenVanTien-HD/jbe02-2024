package com.r2s.springlesson1.controller;

import com.r2s.springlesson1.constant.Role;
import com.r2s.springlesson1.dto.JwtDto;
import com.r2s.springlesson1.dto.UserDTO;
import com.r2s.springlesson1.service.UserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/auth")
@RequiredArgsConstructor
public class AuthController {
    private final UserService userService;

    // Response: ResponseEntity<>
    @PostMapping("/signup")
    public ResponseEntity<UserDTO> signup(@Valid @RequestBody UserDTO userDTO) {
        return ResponseEntity.ok(userService.register(userDTO));
    }

    @PostMapping("/login")
    public ResponseEntity<JwtDto> login(@RequestBody UserDTO loginDTO) {
        return ResponseEntity.ok(userService.login(loginDTO));
    }
}
