package com.r2s.springlesson1.controller;

import com.r2s.springlesson1.dto.StudentDto;
import com.r2s.springlesson1.dto.common.BaseResponse;
import com.r2s.springlesson1.dto.paging.PageDto;
import com.r2s.springlesson1.dto.paging.StudentSearchRequestDto;
import com.r2s.springlesson1.entity.StudentEntity;
import com.r2s.springlesson1.service.StudentService;
import com.r2s.springlesson1.utils.ResponseFactory;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/student")
@Slf4j
public class StudentController {

    // SOLID S O L I D
    // DRY

    // Quy tắc: nhận dto trả dto (không sử dụng entity)
    // mapper: ObjectMapper, mapstruct, model mapper
    private final StudentService studentService;

    // IOC: Khi khởi chạy: ứng dụng sẽ quét các class đánh dấu bean
    // Quản lý bean: khởi tạo đẩy vào context, DI, destroy
    // Khởi tạo các đối tượng từ class đó
    // StudentRepository repo = new StudentRepository()
    // StudentMapper mapper =new StudentMapper()
    // StudentServiceImpl serviceImpl = new StudentServiceImpl(repo, mapper);
    // StudentController controller = new StudentController(serviceImpl)

    // Single re

    @Autowired
    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }


    @GetMapping("/list")
    public ResponseEntity<BaseResponse<List<StudentDto>>> getAll() {
        return ResponseFactory.ok(studentService.findAll());
    }

    // Request dispatcher ánh xạ HTTP request vào đúng controller
    @GetMapping("/{id}")
    public ResponseEntity<BaseResponse<StudentDto>> getById(@PathVariable Integer id) {
        return ResponseFactory.ok(studentService.findById(id));
    }

    // request: HTTP POST: /api/v1/student
    // payload: header, body
    // body: {"id": 1, "studentName": "nguyen van tien}
    // =>

    // jackson: student id = 1, studentName = Nguyen Vna Tien
    @PostMapping
    public StudentEntity insert(@Valid @RequestBody StudentEntity student) {
        return studentService.insert(student);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<BaseResponse<Void>> delete(@PathVariable("id") Long id) {
        log.info("Xoa theo id {}", id);

        // xuong day la thanh cong
        Void v = null; // mock service method
        return ResponseFactory.ok(null);
    }

    // todo: demo search
    @GetMapping("/search")
    public ResponseEntity<BaseResponse<PageDto<StudentDto>>> search(
            @Valid @RequestBody StudentSearchRequestDto requestDto) {
        return ResponseFactory
                .ok(studentService.search(requestDto));
    }
}
