package com.r2s.springlesson1.service.impl;

import com.r2s.springlesson1.entity.ClassroomEntity;
import com.r2s.springlesson1.repository.ClassroomRepository;
import com.r2s.springlesson1.service.ClassroomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClassroomServiceImpl implements ClassroomService {
    private final ClassroomRepository classroomRepository;

    @Autowired
    public ClassroomServiceImpl(ClassroomRepository classroomRepository) {
        this.classroomRepository = classroomRepository;
    }


    @Override
    public List<ClassroomEntity> getAll() {
        return classroomRepository.findAll();
    }
}
