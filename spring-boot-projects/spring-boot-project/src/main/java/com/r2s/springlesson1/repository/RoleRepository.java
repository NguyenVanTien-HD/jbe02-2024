package com.r2s.springlesson1.repository;

import com.r2s.springlesson1.entity.RoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Set;

public interface RoleRepository extends JpaRepository<RoleEntity, Long> {
    Set<RoleEntity> findAllByRoleNameIn(List<String> roleNames);
}
