package com.r2s.springlesson1.mapper;

import com.r2s.springlesson1.constant.Gender;
import com.r2s.springlesson1.dto.StudentDto;
import com.r2s.springlesson1.entity.StudentEntity;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class StudentMapper {

    private final ModelMapper mapper;

    public StudentMapper(ModelMapper mapper) {
        this.mapper = mapper;
    }

    public StudentDto toDto(StudentEntity entity) {
        StudentDto dto = new StudentDto();
        dto.setStudentId(entity.getStudentId());

        if (entity.getGender().equals(Gender.MALE.getValue())) {
            dto.setGenderDisplay(Gender.MALE.getDisplayString());
        } else {
            dto.setGenderDisplay(Gender.FEMALE.getDisplayString());
        }

        // ...
        return dto;
    }

    public StudentDto mapped(StudentEntity entity) {
        StudentDto dto = mapper.map(entity, StudentDto.class);
        if (entity.getGender().equals(Gender.MALE.getValue())) {
            dto.setGenderDisplay(Gender.MALE.getDisplayString());
        } else {
            dto.setGenderDisplay(Gender.FEMALE.getDisplayString());
        }
        return dto;
    }
}
