package com.r2s.springlesson1.service.impl;

import com.r2s.springlesson1.constant.Role;
import com.r2s.springlesson1.dto.JwtDto;
import com.r2s.springlesson1.dto.UserDTO;
import com.r2s.springlesson1.entity.RoleEntity;
import com.r2s.springlesson1.entity.UserEntity;
import com.r2s.springlesson1.exception.AlreadyExistsException;
import com.r2s.springlesson1.exception.InvalidCredentialsException;
import com.r2s.springlesson1.repository.RoleRepository;
import com.r2s.springlesson1.repository.UserRepository;
import com.r2s.springlesson1.security.CustomUserDetails;
import com.r2s.springlesson1.security.JwtTokenProvider;
import com.r2s.springlesson1.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final AuthenticationManager authenticationManager;
    private final JwtTokenProvider jwtTokenProvider;
    private final RoleRepository roleRepository;

    @Override
    @Transactional
    public UserDTO register(UserDTO user) {

        if (userRepository.existsByUsername(user.getUsername())) {
            throw new AlreadyExistsException("User", "username", user.getUsername());
        }

        // create user
        UserEntity entity = new UserEntity();
        entity.setPassword(passwordEncoder.encode(user.getPassword()));
        entity.setUsername(user.getUsername());
        entity.setRoles(roleRepository.findAllByRoleNameIn(user.getRoles()));

        UserEntity savedUser = userRepository.save(entity);
        user.setId(savedUser.getId());

        return user;
    }

    public JwtDto login(UserDTO user) {
        try {
            // Perform authentication
            // 1. pass encoder hash thong password truyen vao
            // 2. userDetail service lay thong tin tu trong db theo username
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword()));

            // Retrieve user details from the authenticated token
            CustomUserDetails userDetails = (CustomUserDetails) authentication.getPrincipal();

            // Generate JWT token
            String accessToken = jwtTokenProvider.generateToken(userDetails);
            Date expriedDate = jwtTokenProvider.extractExpiration(accessToken);

            return JwtDto.builder()
                    .accessToken(accessToken)
                    .expiredIn(expriedDate)
                    .build();
        } catch (AuthenticationException e) {
            // Handle authentication failure
            log.error("Wrong username or password {}", e.getMessage(), e);
            throw new InvalidCredentialsException("Wrong username or password");
        }
    }

}
