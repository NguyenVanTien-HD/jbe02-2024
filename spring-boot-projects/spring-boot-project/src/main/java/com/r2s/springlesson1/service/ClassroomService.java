package com.r2s.springlesson1.service;

import com.r2s.springlesson1.entity.ClassroomEntity;

import java.util.List;

public interface ClassroomService {
    List<ClassroomEntity> getAll();
}
