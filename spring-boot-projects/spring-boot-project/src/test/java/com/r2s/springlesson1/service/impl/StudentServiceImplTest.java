//package com.r2s.springlesson1.service.impl;
//
//import com.r2s.springlesson1.dto.StudentDto;
//import com.r2s.springlesson1.entity.StudentEntity;
//import com.r2s.springlesson1.exception.CommonException;
//import com.r2s.springlesson1.mapper.StudentMapper;
//import com.r2s.springlesson1.repository.StudentRepository;
//import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.mockito.Mockito;
//
//import java.util.Optional;
//
//import static org.mockito.Mockito.when;
//
//class StudentServiceImplTest {
//    private StudentRepository studentRepository;
//    private StudentMapper studentMapper;
//
//    private StudentServiceImpl studentService;
//
//    @BeforeEach
//    void setUp() {
//        this.studentRepository = Mockito.mock(StudentRepository.class);
//        this.studentMapper = Mockito.mock(StudentMapper.class);
//        this.studentService = new StudentServiceImpl(studentRepository, studentMapper);
//    }
//
//    @Test
//    void findAll() {
//    }
//
//    @Test
//    void findById_success() {
//        Integer id = 1;
//        // mock entity
//        StudentEntity entity = new StudentEntity();
//        entity.setStudentId(id);
//        entity.setStudentName("tiennv");
//        // mock dto
//        StudentDto dto = new StudentDto();
//        dto.setStudentId(entity.getStudentId());
//        dto.setStudentName(entity.getStudentName());
//
//        when(studentRepository.findById(id)).thenReturn(Optional.of(entity));
//        when(studentMapper.mapped(entity)).thenReturn(dto);
//
//        // actual
//        StudentDto actual = studentService.findById(1);
//
//        // assertion
//        Integer studentIdExpected = 1;
//        String studentNameExpected = "tiennv";
//
//        Assertions.assertEquals(studentIdExpected, actual.getStudentId());
//        Assertions.assertEquals(studentNameExpected, actual.getStudentName());
//    }
//
//    @Test
//    void findById_has_ex() {
//        Integer id = 1;
//
//        // mock data return empty
//        when(studentRepository.findById(id)).thenReturn(Optional.empty());
//
//        // actual
//        Assertions.assertThrows(CommonException.class, () -> studentService.findById(id));
//    }
//
//    @Test
//    void insert() {
//    }
//}
