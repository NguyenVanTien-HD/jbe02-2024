package com.r2s.springlesson1.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.r2s.springlesson1.dto.JwtDto;
import com.r2s.springlesson1.dto.UserDTO;
import com.r2s.springlesson1.service.UserService;
import net.bytebuddy.matcher.ElementMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class AuthControllerJTest {
    private UserService userService;
    private AuthController authController;

    @BeforeEach
    void setUp() {
        this.userService = Mockito.mock(UserService.class);
        this.authController = new AuthController(userService);
    }

//    @Test
//    void signup() {
//        // give userService.register(userDTO)
//        UserDTO request = new UserDTO();
//        request.setUsername("tiennv");
//        request.setPassword("12345");
//        request.setRoles(List.of("ADMIN", "USER"));
//
//        UserDTO responseService = new UserDTO();
//        responseService.setUsername(request.getUsername());
//        responseService.setPassword(request.getPassword());
//        Mockito.when(userService.register(any())).thenReturn(responseService);
//
//        // execute
//        var actual = authController.signup(request);
//
//        // expected - actual
//        String usernameExpected = "tiennv";
//        String passExpected = "12345";
//
//
//        assertEquals(HttpStatus.OK, actual.getStatusCode());
//        assertEquals(HttpStatus.OK, actual.getStatusCode());
//        assertNotNull(actual.getBody());
//        assertEquals(usernameExpected, actual.getBody().getUsername());
//        assertEquals(passExpected, actual.getBody().getPassword());
//    }

    @Test
    void login_test() {
        // input
        UserDTO loginDTO = new UserDTO();
        loginDTO.setUsername("tiennv");
        loginDTO.setPassword("12345");

        // mock output dependency
        JwtDto jwtDto = JwtDto.builder()
                .accessToken("token")
                .expiredIn(new Date())
                .build();

        when(userService.login(loginDTO)).thenReturn(jwtDto);

        // execute
        ResponseEntity<JwtDto> actual = authController.login(loginDTO);


        // assert
        assertEquals(HttpStatus.OK, actual.getStatusCode());
        assertNotNull(actual.getBody());
        assertEquals("token", actual.getBody().getAccessToken());
    }
}
